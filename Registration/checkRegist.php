<?php

require_once '../Config.php';

require_once (Config::getApplicationManagerPath() . 'UnregisteredUserManager.php');
require_once (Config::getApplicationModelPath() . 'UnregisteredUser.php');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$errors = array();

if (filter_has_var(INPUT_POST, 'RegistUser')) {
    $user = filter_input(INPUT_POST, 'RegistUser', FILTER_SANITIZE_STRING, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    if (strlen($user) < 3 || strlen($user) > 20) {
        $errors['User'] = "Nome deverá ter entre 3 a 20 caracteres.";
    } else if (!preg_match("/^[a-z0-9A-Z ]*$/", $user)) {
        $errors['User'] = "Só letras, numeros e espaços";
    }
}


if (filter_has_var(INPUT_POST, 'RegistPass')) {
    $pass = filter_input(INPUT_POST, 'RegistPass', FILTER_SANITIZE_STRING, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    if (strlen($pass) < 3 || strlen($pass) > 20) {
        $errors['pass'] = "Password deverá ter entre 3 a 20 caracteres.";
    }
}

if (filter_has_var(INPUT_POST, 'RegistcheckPass')) {
    $passcheck = filter_input(INPUT_POST, 'RegistcheckPass', FILTER_SANITIZE_STRING, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    if (strlen($passcheck) < 3 || strlen($passcheck) > 20) {
        $errors['pass'] = "Password deverá ter entre 3 a 20 caracteres.";
    } else if (strcasecmp($pass, $passcheck)) {
        $erros['passcheck'] = "A password nao é igual.";
    }
}

if (filter_has_var(INPUT_POST, 'RegistEmail')) {
    $email = filter_input(INPUT_POST, 'RegistEmail', FILTER_SANITIZE_EMAIL,FILTER_SANITIZE_SPECIAL_CHARS);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors['email'] = "Email inválido.";
    }
}

if (filter_has_var(INPUT_POST, 'RegistPais')) {
    $pais = filter_input(INPUT_POST, 'RegistPais');
    if (!($pais !== "DE" || $pais !== "GB" || $pais !== "ZH" || $pais !== "KON" || $pais !== "KOS" || $pais !== "HR" || $pais !== "DA" || $pais !== "ES" || $pais !== "ET" || $pais !== "FI" || $pais !== "FR" || $pais !== "EL" || $pais !== "NL" || $pais !== "HU" || $pais !== "EN" || $pais !== "IS" || $pais !== "ID" || $pais !== "GA" || $pais !== "IT" || $pais !== "JA" || $pais !== "LT" || $pais !== "MK" || $pais !== "MT" || $pais !== "NO" || $pais !== "PL" || $pais !== "PT" || $pais !== "RO" || $pais !== "RU" || $pais !== "SR" || $pais !== "SV" || $pais !== "TR" || $pais !== "UK")) {
        $errors['country'] = "País inválido.";
    }
}

if(filter_has_var(INPUT_POST,'RegistAddress')){
    $address = filter_input(INPUT_POST,'RegistAddress',FILTER_SANITIZE_SPECIAL_CHARS);
    if(empty($address)){
        $errors['address'] = "Morada inválida";
    }
}

if(filter_has_var(INPUT_POST,'RegistTel')){
    $tel = filter_input(INPUT_POST,'RegistTel',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_VALIDATE_INT);
    if(strlen($tel) !== 9){
        $errors['tel'] = "Telefone inválido";
    }
}

if (isset($errors)) {
    if (count($errors) === 0) {
        $newpass = password_hash($pass,PASSWORD_DEFAULT);
        $newemail = password_hash($email,PASSWORD_DEFAULT);
        $newuser = new UnregisteredUser($user, $newpass, $newemail,$pais,$address,$tel);
        $newman = new UnregisteredUserManager();
        $newman->insertUser($newuser);
        header('location:../index.php');
    } else {
        header('location:Regist.php?registerror=true');
    }
}