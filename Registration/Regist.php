<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registo</title>
        <link href="../styles/styleHome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/RegistJS.js" type="text/javascript"></script>
    </head>
    <body>
        <header>
            <h1>Registo</h1>
        </header>
        <div id="main"><?php
            if (filter_has_var(INPUT_GET, 'registerror') == TRUE) {
                echo 'Registo não concluído!';
            }
            ?>
            <form method="POST" action="checkRegist.php" id="formRegist">
                <label><input type="text" placeholder="Insira um username.." name="RegistUser" required maxlength="15" class="inputtype" id="RegistName"></label><span id="nameerror" class="error"></span>
                <label><input type="password" placeholder="Insira uma password.." name="RegistPass" required maxlength="20" class="inputtype" id="RegistPass"></label><span id="passerror" class="error"></span>
                <label><input type="password" placeholder="Re-insira password" name="RegistcheckPass" required maxlength="20" class="inputtype" id="RegistcheckPass"></label><span id="checkpasserror" class="error"></span>
                <label><input type="email" placeholder="Introduza o seu E-mail.." name="RegistEmail" required class="inputtype" id="RegistEmail"></label><span id="emailerror" class="error"></span>
                <label><input type="text" placeholder="Introduza morada" name="RegistAddress" class="inputtype" id="RegistAddress"></label><span id="addresserror" class="error"></span>
                <label><input type="tel" placeholder="Insira numero de telemovel" name="RegistTel" class="inputtype" id="RegistTel"></label><span id="telerror" class="error"></span>
                <select name="RegistPais">
                    <option value="DE">Alemanha</option>
                    <option value="BG">Bulgaria</option>
                    <option value="ZH">China</option>
                    <option value="KON">Coreia do Norte</option>
                    <option value="KOS">Coreia do Sul</option>
                    <option value="HR">Croacia</option>
                    <option value="DA">Dinamarca</option>          
                    <option value="ES">Espanha</option>
                    <option value="ET">Estonia</option>
                    <option value="FI">Finlândia</option>
                    <option value="FR">França</option>
                    <option value="EL">Grecia</option>
                    <option value="NL">Holanda</option>
                    <option value="HU">Hungria</option>
                    <option value="EN">Inglaterra</option>
                    <option value="IS">Islândia</option>
                    <option value="ID">Indonesia</option>
                    <option value="GA">Irlanda</option>
                    <option value="IT">Italia</option>
                    <option value="JA">Japão</option>
                    <option value="LT">Lithuania</option>
                    <option value="MK">Macedonia</option>
                    <option value="MT">Malta</option>
                    <option value="NO">Noruega</option>
                    <option value="PL">Polonia</option>
                    <option value="PT">Portugal</option>
                    <option value="RO">Romenia</option>
                    <option value="RU">Russia</option>
                    <option value="SR">Servia</option>
                    <option value="SV">Suecia </option>
                    <option value="TR">Turquia</option>
                    <option value="UK">Ucrania</option>
                </select>
                <p><input type="submit" value="Ok" class="inputsubmit"></p>
            </form>
            <a href="../index.php" class="submitbutton">Voltar</a>
        </div>
    </body>
</html>
