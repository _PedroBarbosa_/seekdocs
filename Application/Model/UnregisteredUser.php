<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UnregisteredUser
 *
 * @author Runo
 */
class UnregisteredUser {

    private $name;
    private $pass;
    private $email;
    private $country;
    private $address;
    private $tel;

    public function __construct($name, $pass, $email, $country, $address, $tel) {
        $this->name = $name;
        $this->pass = $pass;
        $this->email = $email;
        $this->country = $country;
        $this->address = $address;
        $this->tel = $tel;
    }

    public function getName() {
        return $this->name;
    }

    public function getPass() {
        return $this->pass;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getCountry() {
        return $this->country;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setPass($pass) {
        $this->pass = $pass;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setCountry($country) {
        $this->country = $country;
    }

    public function getAddress() {
        return $this->address;
    }

    public function getTel() {
        return $this->tel;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function setTel($tel) {
        $this->tel = $tel;
    }

    public function convertObjectToArray() {
        $data = array('User' => $this->getName(),
            'Password' => $this->getpass(),
            'email' => $this->getEmail(),
            'Pais' => $this->getCountry(),
            'address' => $this->getAddress(),
            'tel' => $this->getTel());
        return $data;
    }

    public static function convertArrayToObject(Array &$data) {
        return self::createObject($data['User'], $data['Pass'], $data['email'], $data['Pais'], $data['address'], $data['tel']);
    }

    public static function createObject($name, $pass, $email, $country, $address, $tel) {
        $UnregisteredUser = new UnregisteredUser();
        $UnregisteredUser->setName($name);
        $UnregisteredUser->setPass($pass);
        $UnregisteredUser->setEmail($email);
        $UnregisteredUser->setCountry($country);
        $UnregisteredUser->setAddress($address);
        $UnregisteredUser->setTel($tel);

        return $UnregisteredUser;
    }

}
