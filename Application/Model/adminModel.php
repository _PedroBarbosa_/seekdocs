<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of adminModel
 *
 * @author Runo
 */
class adminModel {

    private $name;
    private $pass;

    public function __construct($name, $pass) {
        $this->name = $name;
        $this->pass = $pass;
    }

    public function getName() {
        return $this->name;
    }

    public function getPass() {
        return $this->pass;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setPass($pass) {
        $this->pass = $pass;
    }

    public function convertObjectToArray() {
        $data = array('name' => $this->getName(),
                       'pass' => $this->getPass());

        return $data;
    }

    public static function convertArrayToObject(Array &$data) {
        return self::createObject($data['Name'], $data['Pass']);
    }

    public static function createObject($name, $pass) {
        $Admin = new adminModel();
        $Admin->setName($name);
        $Admin->setPass($pass);

        return $Admin;
    }

}
