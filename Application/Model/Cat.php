<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cat
 *
 * @author Runo
 */
class Cat {
    
    private $cat;
    
    public function __construct($cat) {
        $this->cat = $cat;
    }
    
    public function getCat() {
        return $this->cat;
    }

    public function setCat($cat) {
        $this->cat = $cat;
    }
    
}
