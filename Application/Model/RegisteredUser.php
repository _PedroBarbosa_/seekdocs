<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RegisteredUser
 *
 * @author Runo
 */
class RegisteredUser {

    private $id;
    private $name;
    private $pass;
    private $email;
    private $country;
    private $address;
    private $tel;
    private $ativo;

    function __construct() {
        
    }

    public function getName() {
        return $this->name;
    }

    public function getPass() {
        return $this->pass;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getCountry() {
        return $this->country;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setPass($pass) {
        $this->pass = $pass;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setCountry($country) {
        $this->country = $country;
    }

    public function getAddress() {
        return $this->address;
    }

    public function getTel() {
        return $this->tel;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function setTel($tel) {
        $this->tel = $tel;
    }

    public function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

    public function getAtivo() {
        return $this->ativo;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function convertObjectToArray() {
        $data = array('ID' => $this->getId(),
            'User' => $this->getName(),
            'Password' => $this->getpass(),
            'email' => $this->getEmail(),
            'Pais' => $this->getCountry(),
            'address' => $this->getAddress(),
            'tel' => $this->getTel(),
            'ativo' => $this->getAtivo());
        return $data;
    }

    public static function convertArrayToObject(Array &$data) {
        return self::createObject($data['ID'], $data['User'], $data['Password'], $data['email'], $data['Pais'], $data['address'], $data['tel']);
    }

    public static function createObject($id, $name, $pass, $email, $country, $address, $tel, $ativo = null) {
        $RegisteredUser = new RegisteredUser();
        $RegisteredUser->setId($id);
        $RegisteredUser->setName($name);
        $RegisteredUser->setPass($pass);
        $RegisteredUser->setEmail($email);
        $RegisteredUser->setCountry($country);
        $RegisteredUser->setAddress($address);
        $RegisteredUser->setTel($tel);
        $RegisteredUser->setAtivo($ativo);

        return $RegisteredUser;
    }

}
