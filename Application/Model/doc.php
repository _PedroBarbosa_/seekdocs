<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of doc
 *
 * @author Runo
 */
class doc {

    private $id;
    private $titulo;
    private $autor;
    private $resumo;
    private $categoria;
    private $conteudo;
    private $tags;
    private $shortcut;
    private $filesize;
    private $publico;
    private $uploader;

    public function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getAutor() {
        return $this->autor;
    }

    public function getResumo() {
        return $this->resumo;
    }

    public function getCategoria() {
        return $this->categoria;
    }

    public function getConteudo() {
        return $this->conteudo;
    }

    public function getTags() {
        return $this->tags;
    }

    public function getShortcut() {
        return $this->shortcut;
    }

    public function getFilesize() {
        return $this->filesize;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setAutor($autor) {
        $this->autor = $autor;
    }

    public function setResumo($resumo) {
        $this->resumo = $resumo;
    }

    public function setCategoria($categoria) {
        $this->categoria = $categoria;
    }

    public function setConteudo($conteudo) {
        $this->conteudo = $conteudo;
    }

    public function setTags($tags) {
        $this->tags = $tags;
    }

    public function setShortcut($shortcut) {
        $this->shortcut = $shortcut;
    }

    public function setFilesize($filesize) {
        $this->filesize = $filesize;
    }

    public function getPublico() {
        return $this->publico;
    }

    public function getUploader() {
        return $this->uploader;
    }

    public function setPublico($publico) {
        $this->publico = $publico;
    }

    public function setUploader($uploader) {
        $this->uploader = $uploader;
    }

    public function convertObjectToArray() {
        $data = array('id' => $this->getId(),
            'Titulo' => $this->getTitulo(),
            'Autor' => $this->getAutor(),
            'Resumo' => $this->getResumo(),
            'Categoria_categoria' => $this->getCategoria(),
            'Conteudo' => $this->getConteudo(),
            'Tags' => $this->getTags(),
            'Shortcut' => $this->getShortcut(),
            'filesize' => $this->getFilesize(),
            'publico' => $this->getPublico(),
            'uploader_id' => $this->getUploader());
        return $data;
    }

    public static function convertArrayToObject(Array &$data) {
        return self::createObject($data['id'], $data['Titulo'], $data['Autor'], $data['Resumo'], $data['Categoria_categoria'], $data['Conteudo'], $data['Tags'], $data['Shortcut'], $data['filesize'], $data['publico'], $data['uploader_id']);
    }

    public static function createObject($id, $titulo, $autor, $resumo, $categoria, $conteudo, $tags, $shortcut, $filesize, $publico, $uploader) {
        $doc = new doc();
        $doc->setId($id);
        $doc->setTitulo($titulo);
        $doc->setAutor($autor);
        $doc->setResumo($resumo);
        $doc->setCategoria($categoria);
        $doc->setConteudo($conteudo);
        $doc->setTags($tags);
        $doc->setShortcut($shortcut);
        $doc->setFilesize($filesize);
        $doc->setPublico($publico);
        $doc->setUploader($uploader);
        return $doc;
    }

}
