<?php

require_once (realpath(dirname(__FILE__)) . '/../../Config.php');

use Config as Conf;

require_once (Conf::getApplicationDatabasePath() . 'MyDataAccessPDO.php');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of registeredDocsManager
 *
 * @author Runo
 */
class registeredDocsManager extends MyDataAccessPDO{
    
    const SQL_TABLE_NAME = 'registedusers_docs';
    
    public function __construct() {
        parent::__construct();
    }

    
    public function getUserDocs($id){
        try{
            return $this->getRecords(self::SQL_TABLE_NAME, array('registedusers_id' => $id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getAllDocs(){
        try{
            return $this->getRecords(self::SQL_TABLE_NAME);
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function insertDoc($docs_id,$reg_id){
        try{
            return $this->insert(self::SQL_TABLE_NAME,array('registedusers_id' => $reg_id,'docs_id' => $docs_id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function removeDoc($doc_id){
        try{
            return $this->delete(self::SQL_TABLE_NAME,array('docs_id' => $doc_id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
     public function SqlQuery($sql) {
        try {
            return $this->getRecordsByUserQuery($sql);
        } catch (Exception $ex) {
            
        }
    }
}
