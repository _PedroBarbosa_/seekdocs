<?php

require_once (realpath(dirname(__FILE__)) . '/../../Config.php');

use Config as Conf;

require_once (Conf::getApplicationDatabasePath() . 'MyDataAccessPDO.php');
require_once (Conf::getApplicationModelPath() . 'RegisteredUser.php');

class RegisteredUserManager extends MyDataAccessPDO {

    const SQL_TABLE_NAME = 'registedusers';

    function __construct() {
        parent::__construct();
    }

    public function getLoginByUser($name) {
        try {
            return $this->getRecords(self::SQL_TABLE_NAME, array('User' => $name));
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getUserById($id) {
        try {
            return $this->getRecords(self::SQL_TABLE_NAME, array('ID' => $id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getAllUsers(){
        try{
            return $this->getRecords(self::SQL_TABLE_NAME);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function insertUser(RegisteredUser $user) {
        try {
            $this->insert(self::SQL_TABLE_NAME, $user->convertObjectToArray());
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function insertUserArray($array = []) {
        try {
            $this->insert(self::SQL_TABLE_NAME, $array);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function EditUserById(RegisteredUser $user) {
        try {
            $this->update(self::SQL_TABLE_NAME, $user->convertObjectToArray(),array('ID' => $user->getId()));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function EditUserArray(array $fields, $id) {
        try {
            return $this->update(self::SQL_TABLE_NAME,$fields,array('ID' => $id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function DeleteUser(RegisteredUser $user) {
        try {
            return $this->delete(self::SQL_TABLE_NAME, array('User' => $user->getUser()));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function DeleteUserByName($name) {
        try {
            return $this->delete(self::SQL_TABLE_NAME, array('User' => $name));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function DeleteUserByID($id) {
        try {
            return $this->delete(self::SQL_TABLE_NAME, array('ID' => $id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function SqlQuery($sql) {
        try {
            return $this->getRecordsByUserQuery($sql);
        } catch (Exception $ex) {
            
        }
    }

    public function CheckAuth($user, $pass) {

        $login = $this->getLoginByUser($user);

        if (count($login) == 1) {
            if (password_verify($pass, $login[0]['Password'])) {
                return true;
            } else {
                return false;
            }
        }
    }

}
