<?php

require_once (realpath(dirname(__FILE__)) . '/../../Config.php');

use Config as Conf;

require_once (Conf::getApplicationDatabasePath() . 'MyDataAccessPDO.php');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of partilhasManager
 *
 * @author Runo
 */
class partilhasManager extends MyDataAccessPDO{

    const SQL_TABLE_NAME = 'partilhas';

    function __construct() {
        parent::__construct();
    }

    public function getPartilhaById($id) {
        try {
            return $this->getRecords(self::SQL_TABLE_NAME, array('ID' => $id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getUserPartilhas($user){
        try{
            return $this->getRecords(self::SQL_TABLE_NAME,array('user' => $user));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    

    public function insertPartilha($docid,$user) {
        try {
            $this->insert(self::SQL_TABLE_NAME,array('doc_id' => $docid,'user' => $user));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function DeletePartilhaById($id) {
        try {
            return $this->delete(self::SQL_TABLE_NAME, array('ID' => $id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function SqlQuery($sql) {
        try {
            return $this->getRecordsByUserQuery($sql);
        } catch (Exception $ex) {
            
        }
    }
}
