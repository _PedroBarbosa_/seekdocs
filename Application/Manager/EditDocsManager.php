<?php

require_once (realpath(dirname(__FILE__)) . '/../../Config.php');

require_once (Config::getApplicationDatabasePath() . 'MyDataAccessPDO.php');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EditDocsManager
 *
 * @author Runo
 */
class EditDocsManager extends MyDataAccessPDO{
    
    const SQL_TABLE_NAME = 'edicaodoc';
    
    public function getEdicao($docid){
        try{
            return $this->getRecords(self::SQL_TABLE_NAME,array('doc_id' => $docid));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function insertEdicao($docid,$razao){
        try{
            return $this->insert(self::SQL_TABLE_NAME,array('doc_id' => $docid,'razao' => $razao));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function SqlQuery($sql) {
        try {
            return $this->getRecordsByUserQuery($sql);
        } catch (Exception $ex) {
            
        }
    }
}
