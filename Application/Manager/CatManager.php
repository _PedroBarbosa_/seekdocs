<?php

require_once (realpath(dirname(__FILE__)) . '/../../Config.php');

use Config as Conf;

require_once (Conf::getApplicationDatabasePath() . 'MyDataAccessPDO.php');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CatManager
 *
 * @author Runo
 */
class CatManager extends MyDataAccessPDO{
    
    const SQL_TABLE_NAME = 'Categorias';
    
    public function __construct() {
        parent::__construct();
    }
    
    public function getAllCat(){
        try{
            return $this->getRecords(self::SQL_TABLE_NAME);
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getCatById($id) {
        try{
            return $this->getRecords(self::SQL_TABLE_NAME, array('id' => $id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getCatByName($name) {
        try{
            return $this->getRecords(self::SQL_TABLE_NAME, array('categoria' => $name));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function insertCat($cat){
        try{
            return $this->insert(self::SQL_TABLE_NAME, array('categoria' => $cat));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function removeCatByCat($cat){
        try{
            return $this->delete(self::SQL_TABLE_NAME,array('categoria' => $cat));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function SqlQuery($sql) {
        try {
            return $this->getRecordsByUserQuery($sql);
        } catch (Exception $ex) {
            
        }
    }

}
