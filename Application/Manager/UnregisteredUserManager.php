<?php

require_once (realpath(dirname(__FILE__)) . '/../../Config.php');

use Config as Conf;

require_once (Conf::getApplicationDatabasePath() . 'MyDataAccessPDO.php');
require_once (Conf::getApplicationModelPath() . 'UnregisteredUser.php');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UnregisteredUserManager
 *
 * @author Runo
 */
class UnregisteredUserManager extends MyDataAccessPDO {

    const SQL_TABLE_NAME = 'unregistedusers';

    function __construct() {
        parent::__construct();
    }

    public function getAlluser() {
        try {
            return $this->getRecords(self::SQL_TABLE_NAME);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function getLoginByUser($name) {
        try {
            return $this->getRecords(self::SQL_TABLE_NAME, array('Registuser' => $name));
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getUserById($id) {
        try {
            return $this->getRecords(self::SQL_TABLE_NAME, array('ID' => $id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function insertUser(UnregisteredUser $user) {
        try {
            $this->insert(self::SQL_TABLE_NAME, $user->convertObjectToArray());
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function EditUserArray(Array $array, $id) {
        try {
            return $this->update(self::SQL_TABLE_NAME, $array, array('id' => $id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function DeleteUser(UnregisteredUser $user) {
        try {
            $this->delete(self::SQL_TABLE_NAME, array('User' => $user->getName()));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function DeleteUserById($id) {
        try {
            return $this->delete(self::SQL_TABLE_NAME, array('ID' => $id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function SqlQuery($sql) {
        try {
            return $this->getRecordsByUserQuery($sql);
        } catch (Exception $ex) {
            
        }
    }

}
