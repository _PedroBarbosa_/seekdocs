<?php

require_once (realpath(dirname(__FILE__)) . '/../../Config.php');

use Config as Conf;

require_once (Conf::getApplicationDatabasePath() . 'MyDataAccessPDO.php');
require_once (Conf::getApplicationModelPath() . 'adminModel.php');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminManager
 *
 * @author Runo
 */
class AdminManager extends MyDataAccessPDO {

    const SQL_TABLE_NAME = 'admin';

    function __construct() {
        parent::__construct();
    }

    public function getAdminByName($name, $pass) {
        try {
            return $this->getRecords(self::SQL_TABLE_NAME, array('name' => $name, 'pass' => $pass));
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function CheckAuth($user, $pass) {

        $login = $this->getAdminByName($user, $pass);

        if (count($login) == 1) {
            return true;
        } else {
            return false;
        }
    }

}
