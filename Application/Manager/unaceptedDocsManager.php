<?php

require_once(realpath(dirname(__FILE__)) . '/../../Config.php');

use Config as Conf;

require_once (Conf::getApplicationDatabasePath() . 'MyDataAccessPDO.php');
require_once (Conf::getApplicationModelPath() . 'doc.php');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of unaceptedDocsManager
 *
 * @author Runo
 */
class unaceptedDocsManager extends MyDataAccessPDO {

    const SQL_TABLE_NAME = 'uncepteddocs';

    public function __construct() {
        parent::__construct();
    }

    public function getAllDocs() {
        try {
            return $this->getRecords(self::SQL_TABLE_NAME);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function getDocByTitle($title) {
        try {
            return $this->getRecords(self::SQL_TABLE_NAME, array('Titulo' => $title));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getDocById($id){
        try{
            return $this->getRecords(self::SQL_TABLE_NAME,array('id' => $id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function insertDoc(uncceptedDoc $doc) {
        try {
            $this->insert(self::SQL_TABLE_NAME, $doc->convertObjectToArray());
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function EditUserArray(Array $array, $id) {
        try {
            return $this->update(self::SQL_TABLE_NAME,$array,array('id' => $id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function DeleteDoc(unaceptedDocsManager $doc) {
        try {
            return $this->delete(self::SQL_TABLE_NAME, array('Titulo' => $doc->getTitulo()));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function DeleteDocById($id) {
        try {
            return $this->delete(self::SQL_TABLE_NAME, array('id' => $id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function SqlQuery($sql) {
        try {
            return $this->getRecordsByUserQuery($sql);
        } catch (Exception $ex) {
            
        }
    }

}
