<?php

require_once (realpath(dirname(__FILE__)) . '/../../Config.php');

use Config as Conf;

require_once (Conf::getApplicationDatabasePath() . 'MyDataAccessPDO.php');
require_once (Conf::getApplicationModelPath() . 'doc.php');

class docmanager extends MyDataAccessPDO {

    const SQL_TABLE_NAME = 'docs';

    function __construct() {
        parent::__construct();
    }

    public function getDocByTitle($title) {
        try {
            return $this->getRecords(self::SQL_TABLE_NAME, array('Titulo' => $title));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getDocByTitlePublic($title) {
        try {
            return $this->getRecords(self::SQL_TABLE_NAME, array('Titulo' => $title,'publico' => 1));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getAlldocs(){
        try{
            return $this->getRecords(self::SQL_TABLE_NAME);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function getDocById($id) {
        try {
            return $this->getRecords(self::SQL_TABLE_NAME, array('id' => $id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function searchAutor($search){
        try{
            return $this->getRecords(self::SQL_TABLE_NAME, array('Autor' => $search));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function searchAutorPublic($search){
        try{
            return $this->getRecords(self::SQL_TABLE_NAME, array('Autor' => $search,'publico' => 1));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getDocsByPrivate1(){
        try{
            return $this->getRecords(self::SQL_TABLE_NAME,array('publico' => 1));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getUserDocs($up_id){
        try{
            return $this->getRecords(self::SQL_TABLE_NAME,array('uploader_id' => $up_id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    

    public function insertDoc(doc $doc) {
        try {
            $this->insert(self::SQL_TABLE_NAME, $doc->convertObjectToArray());
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function EditDoc(doc $doc) {
      try{
          $this->update(self::SQL_TABLE_NAME,$doc->convertObjectToArray(),array('id' => $doc->getId()));
      } catch (Exception $ex) {
          throw $ex;
      }
    }

    public function DeleteDoc(doc $doc) {
        try {
            return $this->delete(self::SQL_TABLE_NAME, array('Titulo' => $doc->getTitulo()));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function DeleteDocById($id) {
        try {
            return $this->delete(self::SQL_TABLE_NAME, array('id' => $id));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function SqlQuery($sql) {
        try {
            return $this->getRecordsByUserQuery($sql);
        } catch (Exception $ex) {
            
        }
    }

}
