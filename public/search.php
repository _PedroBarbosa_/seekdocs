<?php
session_start();

require_once '../Application/Manager/docmanager.php';
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Procurar</title>
        <link href="../styles/Mainstyle.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <header>
            <?php if (array_key_exists('User', $_SESSION)) { ?>
                <a href="../Logout.php">Logout</a>
            <?php } else { ?>
                <a href="../index.php">Log in</a>
            <?php } ?>
        </header>
        <nav>
            <a href="../Home.php">Home</a>
            <a href="Categorias.php">Categorias</a>
            Procurar:
            <form method="GET" action="">
                <input type="search" name="searchdocs" class="searchdocs">
            </form>
        </nav>
        <div id="main">
            <?php
            $searchdocs = filter_input(INPUT_GET, 'searchdocs');

            $docman = new docmanager();
            $datatitle = $docman->getDocByTitlePublic($searchdocs);
            $dataautor = $docman->searchAutorPublic($searchdocs);


            if (empty($dataautor) && empty($datatitle)) {
                echo 'Nenhum resultado a apresentar';
            } else {
                ?>
                <h3>Resultados:</h3>
                <?php
                if (empty($datatitle)) {
                    for ($i = 0; i < count($dataautor); $i++) {
                        ?>
                        <article>
                            <p>Titulo: <b><?= $dataautor[$i]['Titulo'] ?></b></p>
                            <p>Autor: <b><?= $dataautor[$i]['Autor'] ?></b></p>
                            <p>Resumo: <b><?= $dataautor[$i]['Resumo'] ?></b></p>
                            <p>Categoria: <b><?= $dataautor[$i]['Categoria_categoria'] ?></b></p>
                            <p>Data criacao: <b><?= $dataautor[$i]['DataCriacao'] ?></b></p>
                            <p>Filesize: <b><?= $dataautor[$i]['filesize'] ?> kb</b></p>
                            <div class="details">
                                <a href="../Ver.php?docid=<?= $dataautor[$i]['ID'] ?>">Ver detalhes</a>
                            </div>
                        </article>
                    <?php } ?>
                    <?php
                }
                if (empty($dataautor)) {
                    for ($i = 0; $i < count($datatitle); $i++) {
                        ?>
                        <article>
                            <p>Titulo: <b><?= $datatitle[$i]['Titulo'] ?></b></p>
                            <p>Autor: <b><?= $datatitle[$i]['Autor'] ?></b></p>
                            <p>Resumo: <b><?= $datatitle[$i]['Resumo'] ?></b></p>
                            <p>Categoria: <b><?= $datatitle[$i]['Categoria_categoria'] ?></b></p>
                            <p>Data criacao: <b><?= $datatitle[$i]['DataCriacao'] ?></b></p>
                            <p>Filesize: <b><?= $datatitle[$i]['filesize'] ?> kb</b></p>
                            <div class="details">
                                <a href="../Ver.php?docid=<?= $datatitle[$i]['id'] ?>">Ver detalhes</a>
                            </div>
                        </article>
                    <?php } ?>
                <?php
                }
            }
            ?>
        </div>
    </body>
</html>
