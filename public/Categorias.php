<?php
session_start();

require_once '../Application/Manager/CatManager.php';
require_once '../Application/Manager/docmanager.php';
?><!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Categorias</title>
        <link href="../styles/Mainstyle.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <header>
            <?php if (array_key_exists('User', $_SESSION)) { ?>
                <a href="../Logout.php">Logout</a>
            <?php } else { ?>
                <a href="../index.php">Log in</a>
            <?php } ?>
        </header>
        <nav>
                <a href="../Home.php">Home</a>
                <a href="Categorias.php">Categorias</a>
                Procurar:
                <form method="GET" action="search.php">
                    <input type="search" name="searchdocs" class="searchdocs">
                </form>
        </nav>
        <div id="main">
            <?php
            $docsman = new docmanager();
            $datadocs = $docsman->getDocsByPrivate1();

            $catman = new CatManager();
            $data = $catman->getAllCat();

            for ($i = 0; $i < count($data); $i++) {
                ?>
                <h3><?= $data[$i]['categoria'] ?></h3>
                <?php
                for ($j = 0; $j < count($datadocs); $j++) {
                    if ($data[$i]['categoria'] == $datadocs[$j]['Categoria_categoria']) {
                        ?>
                        <article>
                            <p>Titulo: <b><?= $datadocs[$j]['Titulo'] ?></b></p>
                            <p>Autor: <b><?= $datadocs[$j]['Autor'] ?></b></p>
                            <p>Resumo: <b><?= $datadocs[$j]['Resumo'] ?></b></p>
                            <p>Categoria: <b><?= $datadocs[$j]['Categoria_categoria'] ?></b></p>
                            <p>Data criacao: <b><?= $datadocs[$j]['DataCriacao'] ?></b></p>
                            <p>Filesize: <b><?= $datadocs[$j]['filesize'] ?></b></p>
                            <div class="details">
                                <a href="../Ver.php?docid=<?= $datadocs[$i]['id'] ?>">Ver detalhes</a>
                            </div>
                        </article>
                        <?php
                    }
                }
            }
            ?>
        </div>
    </body>
</html>
