<?php
session_start();

if (isset($_SESSION['User']) && $_SESSION['User'] != '') {
    if (array_key_exists('User', $_SESSION)) {
        
    }
} else {
    header('location:../index.php?gtfo=yes');
}

require_once '../Application/Manager/RegisteredUserManager.php';
require_once '../Application/Manager/CatManager.php';

$catman = new CatManager();
$data = $catman->getAllCat();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Inserir Doc</title>
        <link href="../styles/Mainstyle.css" rel="stylesheet" type="text/css"/>
        <script src="../js/inserirDocsJS.js" type="text/javascript"></script>
    </head>
    <body>
        <header>
            <?php if (array_key_exists('User', $_SESSION)) { ?>
                <a href="../Logout.php">Logout</a>
            <?php } else { ?>
                <a href="index.php">Log in</a>
            <?php } ?>
        </header>
        <nav>
            <a href="../Home.php">Home</a>
            <a href="InserirDoc.php">Inserir Doc</a>
            <a href="Perfil.php">Perfil</a>
            <a href="gerirDocsUser.php">Gerir meus docs</a>
            <a href="DocsUserpartilhados.php">Documentos partilhados</a>
            Procurar:
            <form method="GET" action="search.php">
                <input type="search" name="searchdocsUser" class="searchdocs">
            </form>
        </nav>
        <div id="main">
            <h3>Inserir documento</h3><?php if (filter_input(INPUT_GET, 'insertfail') == TRUE) { ?>
                <span class="alert">O documento nao foi inserido por erros</span>
            <?php }if (filter_input(INPUT_GET, 'filecopied') == TRUE) { ?>
                <span class="alert">Ficheiro com esse nome já existe!</span>
            <?php }if (filter_input(INPUT_GET, 'filesize') == TRUE) { ?>
                <span class="alert">Ficheiro demasiado grande</span>
            <?php }if (filter_input(INPUT_GET, 'filetype') == TRUE) { ?>
                <span class="alert">Tipo de ficheiro não suportado</span>
            <?php }if (filter_input(INPUT_GET, 'insert') == TRUE) { ?>
                <span class="alert">Documento inserido com sucesso!</span>
            <?php }if (filter_input(INPUT_GET, 'insertfile') == TRUE) { ?>
                <span class="alert">Escolha documento para upload</span>
            <?php } ?>
            <form method="POST" action="checkDoc.php" enctype="multipart/form-data">
                <label>Autor: <input type="text" name="Autorinput" class="insertdoc" id="Autorinput"></label><span id="autorerror" class="error"></span>
                <label>Categoria: <select class="insertdoc" name="categoriainput">
                        <?php for ($i = 0; $i < count($data); $i++) { ?>
                            <option value="<?= $data[$i]['categoria'] ?>"><?= $data[$i]['categoria'] ?></option>
                        <?php } ?>
                    </select></label><span id="categoriaerror" class="error"></span>

                <label>Partilhar com: (separar por virgulas)<input type="text" name="partilha" class="insertdoc" id="partilha"></label>
                <label>Tags: (separar por virgulas) <input type="text" name="Tagsinput" class="insertdoc" id="tagsinput"></label><span id="tagserror" class="error"></span>
                <input type="hidden" name="public" value="0">
                <label>público: <input type="checkbox" name="public" class="insertdoc" value="1" id="publico"></label>
                <fieldset>
                    <legend>Upload</legend>
                    <p>Tamanho máximo: 100 kB</p>
                    <input type="file" name="fileinput" id="fileinput"><span id="fileerror" class="insertdoc"></span>
                    <p></p>

                </fieldset>
                <input type="submit" value="submeter" class="insertdoc">
            </form>
        </div>
    </body>
</html>
