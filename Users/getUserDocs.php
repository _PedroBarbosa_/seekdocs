<?php
session_start();

require_once '../Application/Manager/docmanager.php';
require_once '../Application/Manager/registeredDocsManager.php';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$id = $_SESSION['ID'];

$docman = new docmanager();

$data = $docman->getUserDocs($id);

echo json_encode($data);
