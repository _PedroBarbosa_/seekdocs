<?php
session_start();

if (isset($_SESSION['User']) && $_SESSION['User'] != '') {
    if (array_key_exists('User', $_SESSION)) {
        
    }
} else {
    header('location:../index.php?gtfo=yes');
}

require_once '../Config.php';

require_once (Config::getApplicationManagerPath() . 'RegisteredUserManager.php');
require_once (Config::getApplicationModelPath() . 'RegisteredUser.php');

$id = filter_var($_SESSION['ID'], FILTER_SANITIZE_NUMBER_INT);

$Userman = new RegisteredUserManager();
$RegUser = new RegisteredUser();

$data = $Userman->getUserById($id);

$obj = $RegUser->convertArrayToObject($data[0]);
$obj->setAtivo(1);

$errors = [];

if (filter_input(INPUT_POST, 'EditName')) {
    if (strcmp($data[0]['User'], filter_input(INPUT_POST, 'EditName')) != 0 && strlen(filter_input(INPUT_POST, 'EditName')) > 0) {
        $nameEdit = filter_input(INPUT_POST, 'EditName', FILTER_SANITIZE_STRING);
        $obj->setName($nameEdit);
    } else {
        $errors['name'] = "Nome inválido";
    }
}

if (filter_input(INPUT_POST, 'EditPass')) {
    if (strlen(filter_input(INPUT_POST, 'EditPass')) > 0) {
        $passedit = filter_input(INPUT_POST, 'EditPass', FILTER_SANITIZE_SPECIAL_CHARS);
        $encpassedit = password_hash($passedit, PASSWORD_DEFAULT);
        $obj->setPass($encpassedit);
    } else {
        $errors['pass'] = "Password inválida";
    }
}

if (filter_input(INPUT_POST, 'EditPass') != filter_input(INPUT_POST, 'checkPass')) {
    $errors['checkPass'] = "Passwords não coincidem!";
}

if (filter_input(INPUT_POST, 'EditEmail')) {
    if (strlen(filter_input(INPUT_POST, 'EditEmail')) > 0) {
        $emailedit = filter_input(INPUT_POST, 'EditEmail', FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);
        $encemailedit = password_hash($emailedit, PASSWORD_DEFAULT);
        $$obj->setEmail($encemailedit);
    } else {
        $errors['mail'] = "Email inválido";
    }
}


if (filter_input(INPUT_POST, 'EditPais')) {
    $paisedit = filter_input(INPUT_POST, 'EditPais', FILTER_SANITIZE_STRING);
    $obj->setCountry($paisedit);
} else {
    $errors['pais'] = "Pais inválido";
}


if (filter_input(INPUT_POST, 'EditAddress')) {
    if (strcmp(filter_input(INPUT_POST, 'EditAddress'), $data[0]['address']) && strlen(filter_input(INPUT_POST, 'EditAddress')) > 0) {
        $addressedit = filter_input(INPUT_POST, 'EditAddress', FILTER_SANITIZE_SPECIAL_CHARS);
        $obj->setAddress($addressedit);
    } else {
        $errors['address'] = "Morada inválida";
    }
}

if (filter_input(INPUT_POST, 'EditTel')) {
    $teledit = filter_input(INPUT_POST, 'EditTel', FILTER_SANITIZE_NUMBER_INT);
    $obj->setTel($teledit);
}

if (count($errors) == 0) {
    try {
        $Userman->EditUserById($obj);
    } catch (Exception $ex) {
        throw $ex;
    }
    header('location:../Logout.php?editsuccess=TRUE');
} else {
    if (isset($errors['checkPass'])) {
        header('location:Perfil.php?invalidpass=TRUE');
    } else {
        header('location:Perfil.php?editError=TRUE');
    }
}