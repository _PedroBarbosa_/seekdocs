<?php
session_start();

if (isset($_SESSION['User']) && $_SESSION['User'] != '') {
    if (array_key_exists('User', $_SESSION)) {
        
    }
} else {
    header('location:../index.php?gtfo=yes');
}

require_once '../Application/Manager/RegisteredUserManager.php';
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Perfil</title>
        <link href="../styles/Mainstyle.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <header>
            <?php if (array_key_exists('User', $_SESSION)) { ?>
                <a href="../Logout.php">Logout</a>
            <?php } else { ?>
                <a href="index.php">Log in</a>
            <?php } ?>
        </header>
        <nav>
            <a href="../Home.php">Home</a>
            <a href="InserirDoc.php">Inserir Doc</a>
            <a href="Perfil.php">Perfil</a>
            <a href="gerirDocsUser.php">Gerir meus docs</a>
            <a href="DocsUserpartilhados.php">Documentos partilhados</a>
            Procurar:
            <form method="GET" action="search.php">
                <input type="search" name="searchdocsUser" class="searchdocs">
            </form>
        </nav>
        <div id="main">
            <?php
            if (array_key_exists('User', $_SESSION) && $_SESSION['ativo'] == 1) {
                $newman = new RegisteredUserManager();

                $User = $newman->getLoginByUser($_SESSION['User']);

                if (filter_has_var(INPUT_GET, 'edit') == TRUE) {
                    ?>
                    <span class="alert">alterado com sucesso!</span>
                    <?php
                }
                if (filter_has_var(INPUT_GET, 'editError') == TRUE) {
                    ?>
                    <span class="alert">Não foi possivel fazer alterações</span>
                    <?php
                }
                if(filter_has_var(INPUT_GET,'invalidpass') == TRUE){?>
                    <span class="alert">As passwords não coincidem!</span>
                <?php }
                ?>
                <h3>Perfil</h3>

                <p>Utilizador: <b><?= $User[0]['User'] ?></b></p>
                <p>Password: <b>*******</b></p>
                <p>País: <b><?= $User[0]['Pais'] ?></b></p>
                <p>Morada: <b><?= $User[0]['address'] ?></b></p>
                <p>Telefone: <b><?= $User[0]['tel'] ?></b></p>
                <div id="perfilButtons">
                    <a href="PerfilEdit.php" class="">Editar Perfil</a>
                </div><?php } else { ?>
                <span class="alert">Não tem permissões!</span>
            <?php } ?>
        </div>
    </body>
</html>
