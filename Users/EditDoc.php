<?php
session_start();

if (isset($_SESSION['User']) && $_SESSION['User'] != '') {
    if (array_key_exists('User', $_SESSION)) {
        
    }
} else {
    header('location:../index.php?gtfo=yes');
}

require_once '../Application/Manager/docmanager.php';
require_once '../Application/Manager/CatManager.php';
$dociid = filter_input(INPUT_GET, 'docid', FILTER_SANITIZE_SPECIAL_CHARS);
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Editar documento</title>
        <link href="../styles/Mainstyle.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <header>
            <?php if (array_key_exists('User', $_SESSION)) { ?>
                <a href="../Logout.php">Logout</a>
            <?php } else { ?>
                <a href="index.php">Log in</a>
            <?php } ?>
        </header>
        <nav>
            <a href="../Home.php">Home</a>
            <a href="InserirDoc.php">Inserir Doc</a>
            <a href="Perfil.php">Perfil</a>
            <a href="gerirDocsUser.php">Gerir meus docs</a>
            <a href="DocsUserpartilhados.php">Documentos partilhados</a>
            Procurar:
            <form method="GET" action="search.php">
                <input type="search" name="searchdocsUser" class="searchdocs">
            </form>
        </nav>
        <div id="main">
            <?php if (filter_input(INPUT_GET, 'editsuccess') == TRUE) { ?>
                <span class="alert">Documento editado com sucesso!</span>
            <?php } if (filter_input(INPUT_GET, 'editError') == TRUE) { ?>
                <span class="alert">Erro ao editar documento</span>
                <?php
            }
            $man = new docmanager();
            $data = $man->getDocById($dociid);

            $mancat = new CatManager();
            $cat = $mancat->getAllCat();
            ?>
            <h3>Editar documento</h3>
            <form method="POST" action="EditDoccheck.php?docid=<?= $dociid ?>" enctype="multipart/form-data">
                <label>Autor: <input type="text" name="Autorinput" class="insertdoc" id="Autorinput" value="<?= $data[0]['Autor'] ?>"></label><span id="autorerror" class="error"></span>
                <label>Categoria: <select class="insertdoc" name="categoriainput">
                        <?php for ($i = 0; $i < count($cat); $i++) { ?>
                            <option value="<?= $cat[$i]['categoria'] ?>" <?php if ($cat[$i]['categoria'] == $data[0]['Categoria_categoria']) { ?>selected<?php } ?>><?= $cat[$i]['categoria'] ?></option>
                        <?php } ?>
                    </select></label><span id="categoriaerror" class="error"></span>

                <label>Partilhar com: <input type="text" name="partilha" class="insertdoc" id="partilha"></label>
                <label>Tags: (separar por virgulas) <input type="text" name="Tagsinput" class="insertdoc" id="tagsinput" value="<?= $data[0]['Tags'] ?>"></label><span id="tagserror" class="error"></span>
                <label>Público: <input type="checkbox" name="public" class="insertdoc" value="1" id="publico" <?php if ($data[0]['publico'] == 1) { ?>checked<?php } ?>></label>
                <label>Razão: <input type="text" name="razao" class="insertdoc"></label>
                <fieldset>
                    <legend>Upload</legend>
                    <p>Tamanho máximo: 100 kB</p>
                    <input type="file" name="fileinput" id="fileinput"><span id="fileerror" class="insertdoc"></span>
                    <p></p>
                    <input type="hidden" name="public" value="0">
                </fieldset>
                <input type="submit" value="submeter" class="insertdoc">
            </form>
        </div>
    </body>
</html>
