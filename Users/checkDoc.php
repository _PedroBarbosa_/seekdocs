<?php

session_start();

require_once '../Application/Manager/docmanager.php';
require_once '../Application/Model/doc.php';
require_once './DocxConversion.php';
require_once '../Application/Manager/partilhasManager.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$errors = array();

$doc = new doc();

if (filter_has_var(INPUT_POST, 'Autorinput')) {
    $autor = filter_input(INPUT_POST, 'Autorinput', FILTER_SANITIZE_STRING);
    $doc->setAutor($autor);
    if (strlen($autor) < 3 || strlen($autor) > 30) {
        $errors['autor'] = "O autor deverá ter entre 3 e 30 caracteres";
    }
}

if (filter_has_var(INPUT_POST, 'Tagsinput')) {
    $tags = filter_input(INPUT_POST, 'Tagsinput', FILTER_SANITIZE_STRING);
    $doc->setTags($tags);
    if ($tags === "") {
        $errors['tags'] = "introduza algumas tags";
    }
}

if (filter_has_var(INPUT_POST, 'categoriainput')) {
    $categoria = filter_input(INPUT_POST, 'categoriainput', FILTER_SANITIZE_SPECIAL_CHARS);
    $doc->setCategoria($categoria);
}

if (filter_has_var(INPUT_POST, 'partilha')) {
    $partilha = filter_input(INPUT_POST, 'partilha', FILTER_SANITIZE_SPECIAL_CHARS);
    $partilhaarray = explode(',', $partilha);
}

if (is_uploaded_file($_FILES['fileinput']['tmp_name'])) {

    $targetdir = "../upload/";
    $targetfile = $targetdir . basename($_FILES['fileinput']['name']);
    $imageFileType = pathinfo($targetfile, PATHINFO_EXTENSION);

    if (file_exists($targetfile)) {
        $errors['filecopy'] = "ficheiro ja existe";
        header('location:InserirDoc.php?filecopied=TRUE');
    }

    if ($imageFileType != "docx") {
        $errors['filetype'] = "tipo de ficheiro não suportado";
        header('location:InserirDoc.php?filetype=TRUE');
    }

    if (filesize($_FILES['fileinput']['tmp_name']) > 100000) {
        $errors['filesize'] = "ficheiro demasiado grande";
        header('location:InserirDoc.php?filesize=TRUE');
    }


    move_uploaded_file($_FILES["fileinput"]["tmp_name"], $targetfile);

    $myfile = $targetdir . $_FILES['fileinput']['name'];

    $readoc = new DocxConversion();
    $conteudo = $readoc->read_docxreal($myfile);

    $resumo = substr($conteudo, 0, 100);

    $titulotemp = filter_var($_FILES['fileinput']['name'], FILTER_SANITIZE_SPECIAL_CHARS);
    $tituloarray = explode('.', $titulotemp);
    $titulo = $tituloarray[0];

    $filesize = filesize($_FILES['fileinput']['tmp_name']);

    $doc->setConteudo($conteudo);
    $doc->setFilesize($filesize);
    $doc->setResumo($resumo);
    $doc->setShortcut($myfile);
    $doc->setTitulo($titulo);
} else {
    header('location:InserirDoc.php?insertfile=TRUE');
}

if (filter_input(INPUT_POST, 'public') == 1) {
    $publico = 1;
} else {
    $publico = 0;
}

$doc->setPublico($publico);
$uploader = $_SESSION['ID'];
$doc->setUploader($uploader);

if (count($errors) == 0) {
    try {
        $man = new docmanager();
        $temp = $man->insertDoc($doc);
    } catch (Exception $ex) {
        throw $ex;
    }

    sleep(1);

    if (filter_input(INPUT_POST, 'partilha')) {
        $partman = new partilhasManager();

        $sql = "SELECT MAX(id) FROM docs";
        $maxid = $man->SqlQuery($sql);

        for ($i = 0; $i < count($partilhaarray); $i++) {
            $partman->insertPartilha($maxid[0][0], $partilhaarray[$i]);
        }
    }

    header('location:InserirDoc.php?insert=true');
} else {
    header('location:InserirDoc.php?insertfail=true');
}