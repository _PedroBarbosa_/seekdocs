<?php
session_start();

if (isset($_SESSION['User']) && $_SESSION['User'] != '') {
    if (array_key_exists('User', $_SESSION)) {
        
    }
} else {
    header('location:../index.php?gtfo=yes');
}

require_once '../Application/Manager/docmanager.php';
require_once '../Application/Manager/partilhasManager.php';

$id = filter_var($_SESSION['ID'], FILTER_SANITIZE_NUMBER_INT);
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Documentos partilhados</title>
        <link href="../styles/Mainstyle.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <header>
            <?php if (array_key_exists('User', $_SESSION)) { ?>
                <a href="../Logout.php">Logout</a>
            <?php } else { ?>
                <a href="index.php">Log in</a>
            <?php } ?>
        </header>
        <nav>
            <a href="../Home.php">Home</a>
            <a href="InserirDoc.php">Inserir Doc</a>
            <a href="Perfil.php">Perfil</a>
            <a href="gerirDocsUser.php">Gerir meus docs</a>
            <a href="DocsUserpartilhados.php">Documentos partilhados</a>
            Procurar:
            <form method="GET" action="search.php">
                <input type="search" name="searchdocsUser" class="searchdocs">
            </form>
        </nav>
        <div id="main">
            <?php if (filter_input(INPUT_GET, 'remok') == TRUE) { ?>
                <span class="alert">Partilha removida com sucasso</span>
            <?php
            }

            $man = new docmanager();
            $sql = "SELECT docs.id,docs.uploader_id,partilhas.doc_id,partilhas.user,docs.Titulo,partilhas.ID FROM docs INNER JOIN partilhas WHERE docs.uploader_id=$id AND docs.id=partilhas.doc_id";
            $data = $man->SqlQuery($sql);

            for ($i = 0; $i < count($data); $i++) {
                ?>
                <article>
                    <p><b>Titulo: </b><?= $data[$i]['Titulo'] ?></p>
                    <p><b>Utilizador: </b><?= $data[$i]['user'] ?></p>
                    <div class="details">
                        <a href="rempartilha.php?id=<?= $data[$i]['ID'] ?>" class="">Remover partilha</a>
                    </div>
                </article>
            <?php }
            ?>
        </div>
    </body>
</html>
