<?php

session_start();

if (isset($_SESSION['User']) && $_SESSION['User'] != '') {
    if (array_key_exists('User', $_SESSION)) {
        
    }
} else {
    header('location:../index.php?gtfo=yes');
}

require_once '../Config.php';

require_once (Config::getApplicationManagerPath() . 'docmanager.php');
require_once (Config::getApplicationModelPath() . 'doc.php');
require_once '../Users/DocxConversion.php';
require_once '../Application/Manager/EditDocsManager.php';

$docid = filter_input(INPUT_GET, 'docid');

$docman = new docmanager();

$data = $docman->getDocById($docid);

$doc = new doc();
$obj = $doc->convertArrayToObject($data[0]);

$errors = [];
if (filter_input(INPUT_POST, 'Autorinput')) {
    if (strlen(filter_input(INPUT_POST, 'Autorinput')) > 0) {
        $autorEdit = filter_input(INPUT_POST, 'Autorinput', FILTER_SANITIZE_STRING);
        $obj->setAutor($autorEdit);
    } else {
        $errors['autor'] = "autor inválido";
    }
}

if (filter_input(INPUT_POST, 'categoriainput')) {
    $catedit = filter_input(INPUT_POST, 'categoriainput', FILTER_SANITIZE_SPECIAL_CHARS);
    $obj->setCategoria($catedit);
} else {
    $errors['cat'] = "categoria inválida";
}



if (filter_input(INPUT_POST, 'Tagsinput')) {
    $tagsedit = filter_input(INPUT_POST, 'Tagsinput', FILTER_SANITIZE_STRING);
    $obj->setTags($tagsedit);
} else {
    $errors['tags'] = "tags inválidas";
}

if (is_uploaded_file($_FILES['fileinput']['tmp_name'])) {

    $targetdir = "../upload/";
    $targetfile = $targetdir . basename($_FILES['fileinput']['name']);
    $imageFileType = pathinfo($targetfile, PATHINFO_EXTENSION);

    if (file_exists($targetfile)) {
        $errors['filecopy'] = "ficheiro ja existe";
    }

    if ($imageFileType != "docx") {
        $errors['filetype'] = "tipo de ficheiro não suportado";
    }

    if (filesize($_FILES['fileinput']['tmp_name']) > 100000) {
        $errors['filesize'] = "ficheiro demasiado grande";
    }


    move_uploaded_file($_FILES["fileinput"]["tmp_name"], $targetfile);

    $myfile = $targetdir . $_FILES['fileinput']['name'];

    $readoc = new DocxConversion();
    $conteudo = $readoc->read_docxreal($myfile); //$conteudo = "teste2";  fread($myfile, filesize($_FILES['fileinput']['tmp_name']));
    $obj->setConteudo($conteudo);

    $resumo = substr($conteudo, 0, 100);
    $obj->setResumo($resumo);

    $titulotemp = filter_var($_FILES['fileinput']['name'], FILTER_SANITIZE_SPECIAL_CHARS);
    $tituloarray = explode('.', $titulotemp);
    $titulo = $tituloarray[0];
    $obj->setTitulo($titulo);

    $filesize = filesize($_FILES['fileinput']['tmp_name']);
    $obj->setFilesize($filesize);
}

if(filter_input(INPUT_POST,'razao')){
    $razao = filter_input(INPUT_POST,'razao',FILTER_SANITIZE_FULL_SPECIAL_CHARS,FILTER_SANITIZE_STRING);
}

if (count($errors) == 0) {
    try {
        $docman->EditDoc($obj,$docid);
        
        $newedit = new EditDocsManager();
        $newedit->insertEdicao($docid, $razao);
    } catch (Exception $ex) {
        throw $ex;
    }
    header('location:gerirDocsUser.php?editsuccess=TRUE');
} else {
    header('location:EditDoc.php?editError=TRUE');
}