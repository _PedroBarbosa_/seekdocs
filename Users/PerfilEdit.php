<?php
session_start();

if (isset($_SESSION['User']) && $_SESSION['User'] != '') {
    if (array_key_exists('User', $_SESSION)) {
        
    }
} else {
    header('location:../index.php?gtfo=yes');
}

require_once '../Application/Manager/RegisteredUserManager.php';
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Editar Perfil</title>
        <link href="../styles/Mainstyle.css" rel="stylesheet" type="text/css"/>
        <script src="../js/EditPerfilJS.js" type="text/javascript"></script>
    </head>
    <body>
        <header>
            <?php if (array_key_exists('User', $_SESSION)) { ?>
                <a href="../Logout.php">Logout</a>
            <?php } else { ?>
                <a href="../index.php">Log in</a>
            <?php } ?>
        </header>
        <nav>
            <a href="../Home.php">Home</a>
            <a href="InserirDoc.php">Inserir Doc</a>
            <a href="Perfil.php">Perfil</a>
            <a href="gerirDocsUser.php">Gerir meus docs</a>
            <a href="DocsUserpartilhados.php">Documentos partilhados</a>
            Procurar:
            <form method="GET" action="search.php">
                <input type="search" name="searchdocsUser" class="searchdocs">
            </form>
        </nav>
        <div id="main">
            <?php
            if (array_key_exists('User', $_SESSION) && $_SESSION['ativo'] == 1) {
                $newman = new RegisteredUserManager();

                $User = $newman->getLoginByUser($_SESSION['User']);
                if (filter_input(INPUT_GET, 'EditError') == TRUE) {
                    ?>
                    <span class="alert">Não foi possivel editar.</span>
                <?php }
                ?>
                <h3>Editar Perfil</h3>

                <form method="POST" action="PerfilEditCheck.php">
                    <p>Nome: <b><input type="text" name="EditName" id="EditName"></b></p><span id="nameerror" class="error"></span>
                    <p>Password: <input type="password" name="EditPass" id="EditPass"> </p><span id="passerror" class="error"></span>
                    <p>Confirmar Password: <input type="password" name="checkPass" id="checkPass"><span id="checkpasserror" class="error"></span></p><?php if (filter_input(INPUT_GET, 'passms')) { ?>
                        <span class="error">Passwords não coincidem</span>
                    <?php } ?>
                        <p>Email: <input type="text" name="EditEmail" id="EditEmail"></p><span id="emailerror" class="error"></span>
                    <select name="EditPais">
                        <option value="DE"<?php if ($User[0]['Pais'] == 'DE') { ?>selected<?php } ?>>Alemanha</option>
                        <option value="BG"<?php if ($User[0]['Pais'] == 'BG') { ?>selected<?php } ?>>Bulgaria</option>
                        <option value="ZH"<?php if ($User[0]['Pais'] == 'ZH') { ?>selected<?php } ?>>China</option>
                        <option value="KON"<?php if ($User[0]['Pais'] == 'KON') { ?>selected<?php } ?>>Coreia do Norte</option>
                        <option value="KOS"<?php if ($User[0]['Pais'] == 'KOS') { ?>selected<?php } ?>>Coreia do Sul</option>
                        <option value="HR"<?php if ($User[0]['Pais'] == 'HR') { ?>selected<?php } ?>>Croacia</option>
                        <option value="DA"<?php if ($User[0]['Pais'] == 'DA') { ?>selected<?php } ?>>Dinamarca</option>          
                        <option value="ES"<?php if ($User[0]['Pais'] == 'ES') { ?>selected<?php } ?>>Espanha</option>
                        <option value="ET"<?php if ($User[0]['Pais'] == 'ET') { ?>selected<?php } ?>>Estonia</option>
                        <option value="FI"<?php if ($User[0]['Pais'] == 'FI') { ?>selected<?php } ?>>Finlândia</option>
                        <option value="FR"<?php if ($User[0]['Pais'] == 'FR') { ?>selected<?php } ?>>França</option>
                        <option value="EL"<?php if ($User[0]['Pais'] == 'EL') { ?>selected<?php } ?>>Grecia</option>
                        <option value="NL"<?php if ($User[0]['Pais'] == 'NL') { ?>selected<?php } ?>>Holanda</option>
                        <option value="HU"<?php if ($User[0]['Pais'] == 'HU') { ?>selected<?php } ?>>Hungria</option>
                        <option value="EN"<?php if ($User[0]['Pais'] == 'EN') { ?>selected<?php } ?>>Inglaterra</option>
                        <option value="IS"<?php if ($User[0]['Pais'] == 'IS') { ?>selected<?php } ?>>Islândia</option>
                        <option value="ID"<?php if ($User[0]['Pais'] == 'ID') { ?>selected<?php } ?>>Indonesia</option>
                        <option value="GA"<?php if ($User[0]['Pais'] == 'GA') { ?>selected<?php } ?>>Irlanda</option>
                        <option value="IT"<?php if ($User[0]['Pais'] == 'IT') { ?>selected<?php } ?>>Italia</option>
                        <option value="JA"<?php if ($User[0]['Pais'] == 'JA') { ?>selected<?php } ?>>Japão</option>
                        <option value="LT"<?php if ($User[0]['Pais'] == 'LT') { ?>selected<?php } ?>>Lithuania</option>
                        <option value="MK"<?php if ($User[0]['Pais'] == 'MK') { ?>selected<?php } ?>>Macedonia</option>
                        <option value="MT"<?php if ($User[0]['Pais'] == 'MT') { ?>selected<?php } ?>>Malta</option>
                        <option value="NO"<?php if ($User[0]['Pais'] == 'NO') { ?>selected<?php } ?>>Noruega</option>
                        <option value="PL"<?php if ($User[0]['Pais'] == 'PL') { ?>selected<?php } ?>>Polonia</option>
                        <option value="PT"<?php if ($User[0]['Pais'] == 'PT') { ?>selected<?php } ?>>Portugal</option>
                        <option value="RO"<?php if ($User[0]['Pais'] == 'RO') { ?>selected<?php } ?>>Romenia</option>
                        <option value="RU"<?php if ($User[0]['Pais'] == 'RU') { ?>selected<?php } ?>>Russia</option>
                        <option value="SR"<?php if ($User[0]['Pais'] == 'SR') { ?>selected<?php } ?>>Servia</option>
                        <option value="SV"<?php if ($User[0]['Pais'] == 'SV') { ?>selected<?php } ?>>Suecia </option>
                        <option value="TR"<?php if ($User[0]['Pais'] == 'TR') { ?>selected<?php } ?>>Turquia</option>
                        <option value="UK"<?php if ($User[0]['Pais'] == 'UK') { ?>selected<?php } ?>>Ucrania</option>
                    </select>
                        <p>Morada: <input type="text" name="EditAddress" id="EditAddress"></p><span id="addresserror" class="error"></span>
                        <p>Telefone: <input type="text" name="EditTel" id="EditTel"></p><span id="telerror" class="error"></span>
                    <p><input type="submit" value="Confirmar" class="mainbuttons"></p>
                </form>
            <?php } else { ?>
                <span class="alert">Não tem permissões</span>
            <?php } ?>
        </div>
    </body>
</html>
