<?php
session_start();

if (isset($_SESSION['User']) && $_SESSION['User'] != '') {
    if (array_key_exists('User', $_SESSION)) {
        
    }
} else {
    header('location:../index.php?gtfo=yes');
}

require_once '../Application/Manager/RegisteredUserManager.php';
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Gerir Documentos</title>
        <link href="../styles/Mainstyle.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery-3.2.1.js" type="text/javascript"></script>
        <script src="../js/UserDocs.js" type="text/javascript"></script>
    </head>
    <body>
        <header>
            <?php if (array_key_exists('User', $_SESSION)) { ?>
                <a href="../Logout.php">Logout</a>
            <?php } else { ?>
                <a href="index.php">Log in</a>
            <?php } ?>
        </header>
        <nav>
            <a href="../Home.php">Home</a>
            <a href="InserirDoc.php">Inserir Doc</a>
            <a href="Perfil.php">Perfil</a>
            <a href="gerirDocsUser.php">Gerir meus docs</a>
            <a href="DocsUserpartilhados.php">Documentos partilhados</a>
            Procurar:
            <form method="GET" action="search.php">
                <input type="search" name="searchdocsUser" class="searchdocs">
            </form>
        </nav>
        <div id="main">
            <h3>Meus Documentos</h3>
            <?php if (filter_has_var(INPUT_GET, 'remdoc') == TRUE) { ?>
                <span class="alert">Documento removido com sucesso.</span>
            <?php }if(filter_input(INPUT_GET,'editsuccess') == TRUE){?>
                <span class="alert">Documento editado com sucesso</span>
            <?php } ?>
        </div>
    </body>
</html>
