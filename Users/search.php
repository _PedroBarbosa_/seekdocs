<?php
session_start();

if (isset($_SESSION['User']) && $_SESSION['User'] != '') {
    if (array_key_exists('User', $_SESSION)) {
        
    }
} else {
    header('location:../index.php?gtfo=yes');
}

require_once '../Application/Manager/docmanager.php';
$idsession = $_SESSION['ID'];
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Procurar</title>
        <link href="../styles/Mainstyle.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <header>
            <?php if (array_key_exists('User', $_SESSION)) { ?>
                <a href="../Logout.php">Logout</a>
            <?php } else { ?>
                <a href="../index.php">Log in</a>
            <?php } ?>
        </header>
        <nav>
            <?php if (array_key_exists('User', $_SESSION) && $_SESSION['ativo'] == 1) { ?>
                <a href="../Home.php">Home</a>
                <a href="InserirDoc.php">Inserir Doc</a>
                <a href="Perfil.php">Perfil</a>
                <a href="gerirDocsUser.php">Gerir meus docs</a>
                <a href="DocsUserpartilhados.php">Documentos partilhados</a>
                Procurar:
                <form method="GET" action="">
                    <input type="search" name="searchdocsUser" class="searchdocs">
                </form>
            <?php } else { ?>
                <a href="../Home.php">Home</a>
                <a href="Categorias.php">Categorias</a>
                Procurar:
                <form method="GET" action="">
                    <input type="search" name="searchdocs" class="searchdocs">
                </form>
            <?php } ?>
        </nav>
        <div id="main">
            <?php
            $searchdocs = filter_input(INPUT_GET, 'searchdocsUser');

            $docman = new docmanager();
            $sql = "SELECT * FROM docs WHERE Autor = '{$searchdocs}' OR Titulo = '{$searchdocs}'";
            $filteredSQL = filter_var($sql,FILTER_SANITIZE_STRIPPED);
            $data = $docman->SqlQuery($sql);
            if (empty($data)) {
                ?>
                <h3>Resultados:</h3>
                    <!--<span class="alert">Nenhum resultado a apresentar</span>-->
            <?php } else {
                ?>
                <h3>Resultados:</h3>
                <?php
                for ($i = 0; $i < count($data); $i++) {
                    if ($data[$i]['publico'] == 1 || ($data[$i]['publico'] == 0 && $idsession == $data[$i]['uploader_id'])) {
                        ?>
                        <article>
                            <p>Titulo: <b><?= $data[$i]['Titulo'] ?></b></p>
                            <p>Autor: <b><?= $data[$i]['Autor'] ?></b></p>
                            <p>Resumo: <b><?= $data[$i]['Resumo'] ?></b></p>
                            <p>Categoria: <b><?= $data[$i]['Categoria_categoria'] ?></b></p>
                            <p>Data criacao: <b><?= $data[$i]['DataCriacao'] ?></b></p>
                            <p>Filesize: <b><?= $data[$i]['filesize'] ?> kb</b></p>
                            <div class="details">
                                <a href="Ver.php?docid=<?= $data[$i]['id'] ?>">Ver detalhes</a>
                            </div>
                        </article>
                        <?php
                    } else {
                        
                    }
                }
                ?>
            <?php } ?>
        </div>
    </body>
</html>
