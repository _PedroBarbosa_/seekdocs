<?php
session_start();
session_destroy();
?><!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Home</title>
        <link href="styles/styleHome.css" rel="stylesheet" type="text/css"/>
        <script src="js/loginJS.js" type="text/javascript"></script>
    </head>
    <body>
        <header>
            <h1>Documentos ABC</h1>
        </header>
        <div id="main">
            <img src="Images/icon.png" alt="Logo icon" class="imgHome">
            <form method="POST" action="login/checkLogin.php">
                <?php if (filter_has_var(INPUT_GET, 'errorAuth') == TRUE) { ?>
                    <span class="error">Login incorreto</span><?php
            }if(filter_has_var(INPUT_GET,'deactivated') == TRUE){?>
                    <span class="alert">O utilizador encontra-se desativado</span>
            <?php }if(filter_has_var(INPUT_GET,'editsucces') == TRUE){?>
                    <span class="alert">Dados alterados. Entre com novas credenciais</span>
            <?php } ?>
                <label><input type="text" name="UserLogin" placeholder="Username.." maxlength="15" autofocus required class="inputtype" id="UserLogin"></label><span class="error" id="usererror"></span>
                <label><input type="password" name="PassLogin" placeholder="Password.." maxlength="20" required class="inputtype" id="PassLogin"></label><span class="error" id="passerror"></span>
                <input type="submit" value="Entrar" class="inputsubmit">
            </form>
            <a href="Registration/Regist.php" class="submitbutton">Registar</a>
            <a href="Home.php" class="submitbutton">Convidado</a>
        </div>
        <footer>
            <a href="Administration/Admin.php">
                <img src="Images/admin.png" alt="admin"> 
            </a>
        </footer>
    </body>
</html>
