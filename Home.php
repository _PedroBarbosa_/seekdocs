<?php
session_start();

require_once './Application/Manager/docmanager.php';
?><!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Home</title>
        <link href="styles/Mainstyle.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <header>
            <?php if (array_key_exists('User', $_SESSION)) { ?>
                <a href="Logout.php">Logout</a>
            <?php } else { ?>
                <a href="index.php">Log in</a>
            <?php } ?>
        </header>
        <nav>
            <?php if (array_key_exists('User', $_SESSION) && isset($_SESSION['ativo']) == 1) { ?>
                <a href="Home.php">Home</a>
                <a href="Users/InserirDoc.php">Inserir Doc</a>
                <a href="Users/Perfil.php">Perfil</a>
                <a href="Users/gerirDocsUser.php">Gerir meus docs</a>
                <a href="Users/DocsUserpartilhados.php">Documentos partilhados</a>
                Procurar:
                <form method="GET" action="Users/search.php">
                    <input type="search" name="searchdocsUser" class="searchdocs">
                </form>
                <?php
            } else {
                ?>
                <a href="Home.php">Home</a>
                <a href="public/Categorias.php">Categorias</a>
                Procurar:
                <form method="GET" action="public/search.php">
                    <input type="search" name="searchdocs" class="searchdocs">
                </form>
                <?php
            }
            ?>
        </nav>
        <div id="main">
            <h3>Ultimos docs:</h3>
            <?php
            if (array_key_exists('User', $_SESSION)) {
                if ($_SESSION['ativo'] == 1) {
                    $man2 = new docmanager();
                    $sql = "SELECT * FROM docs WHERE publico=1 OR uploader_id=" . $_SESSION['ID'] . " ORDER BY id DESC LIMIT 5";
                    $filsql = filter_var($sql, FILTER_SANITIZE_STRING, FILTER_SANITIZE_SPECIAL_CHARS);
                    $datadoc2 = $man2->SqlQuery($filsql);
                    for ($i = 0; $i < count($datadoc2); $i++) {
                        ?>
                        <article>
                            <p>Titulo: <b><?= $datadoc2[$i]['Titulo'] ?></b></p>
                            <p>Autor: <b><?= $datadoc2[$i]['Autor'] ?></b></p>
                            <p>Resumo: <b><?= $datadoc2[$i]['Resumo'] ?></b></p>
                            <p>Categoria: <b><?= $datadoc2[$i]['Categoria_categoria'] ?></b></p>
                            <p>Data de criação: <b><?= $datadoc2[$i]['DataCriacao'] ?></b></p>
                            <p>Tamanho: <b><?= $datadoc2[$i]['filesize'] ?> kb</b></p>
                            <div class="details">
                                <a href="Ver.php?docid=<?= $datadoc2[$i]['id'] ?>">Ver detalhes</a>
                            </div>
                        </article>
                        <?php
                    }
                } else {
                    ?>
                    <span class="alert">O utilizador econtra-se desativado!</span>
                    <?php
                }
            } else {
                $man = new docmanager();
                $datadoc = $man->getDocsByPrivate1();
                for ($i = 0; $i < count($datadoc); $i++) {
                    ?>
                    <article>
                        <p>Titulo: <b><?= $datadoc[$i]['Titulo'] ?></b></p>
                        <p>Autor: <b><?= $datadoc[$i]['Autor'] ?></b></p>
                        <p>Resumo: <b><?= $datadoc[$i]['Resumo'] ?></b></p>
                        <p>Categoria: <b><?= $datadoc[$i]['Categoria_categoria'] ?></b></p>
                        <p>Data de criação: <b><?= $datadoc[$i]['DataCriacao'] ?></b></p>
                        <p>Tamanho: <b><?= $datadoc[$i]['filesize'] ?> kb</b></p>
                        <div class="details">
                            <a href="Ver.php?docid=<?= $datadoc[$i]['id'] ?>">Ver detalhes</a>
                        </div>
                    </article>
                <?php } ?>
            <?php } ?>
        </div>
    </body>
</html>
