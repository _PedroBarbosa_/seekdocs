<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once '../Config.php';
require_once '../Application/Manager/RegisteredUserManager.php';

$man = new RegisteredUserManager();
$test = $man->CheckAuth(filter_input(INPUT_POST,'UserLogin'),filter_input(INPUT_POST,'PassLogin'));

if ($test == true){
    session_start();
    $_SESSION['User'] = filter_input(INPUT_POST,'UserLogin');
    $temp = $man->getLoginByUser(filter_input(INPUT_POST,'UserLogin'));
    $_SESSION['ID'] = (int)$temp[0]['ID'];
    $_SESSION['ativo'] = $temp[0]['ativo'];
    header('location:../Home.php');
} else {
    header("location:../index.php?errorAuth=TRUE");
}