<?php

session_start();

$_SESSION = array();

session_destroy();

if (filter_input(INPUT_GET, 'editsuccess') == TRUE) {
    header('location:index.php?editsucces=TRUE');
} else {
    header('location:index.php?logout=success');
}