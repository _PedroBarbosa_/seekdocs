<?php
session_start();

require_once './Application/Manager/docmanager.php';
require_once './Application/Manager/EditDocsManager.php';
require_once './Application/Manager/partilhasManager.php';

$id = filter_input(INPUT_GET, 'docid', FILTER_SANITIZE_NUMBER_INT);
if (array_key_exists('ID', $_SESSION)) {
    $sessionid = filter_var($_SESSION['ID'], FILTER_SANITIZE_NUMBER_INT);
}
?><!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Detalhes</title>
        <link href="styles/Mainstyle.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery-3.2.1.js" type="text/javascript"></script>
        <script src="js/Verdetalhes.js" type="text/javascript"></script>
    </head>
    <body>
        <header>
            <?php if (array_key_exists('User', $_SESSION)) { ?>
                <a href="Logout.php">Logout</a>
            <?php } else { ?>
                <a href="index.php">Log in</a>
            <?php } ?>
        </header>
        <nav>
            <?php if (array_key_exists('User', $_SESSION)) { ?>
                <a href="Home.php">Home</a>
                <a href="Users/InserirDoc.php">Inserir Doc</a>
                <a href="Users/Perfil.php">Perfil</a>
                <a href="Users/gerirDocsUser.php">Gerir meus docs</a>
                <a href="Users/DocsUserpartilhados.php">Documentos partilhados</a>
                Procurar:
                <form method="GET" action="Users/search.php">
                    <input type="search" name="searchdocsUser" class="searchdocs">
                </form>
            <?php } else { ?>
                <a href="Home.php">Home</a>
                <a href="public/Categorias.php">Categorias</a>
                Procurar:
                <form method="GET" action="public/search.php">
                    <input type="search" name="searchdocs" class="searchdocs">
                </form>
            <?php } ?>
        </nav>
        <div id="main">
            <h3>Detalhes</h3>
            <?php
            $docman = new docmanager();
            $doc = $docman->getDocById($id);

            $edits = new EditDocsManager();
            $dataedits = $edits->getEdicao($id);

            $part = new partilhasManager();
            $datapart = $part->getPartilhaById($id);
            $teste = FALSE;
            for ($i = 0; $i < count($datapart); $i++) {
                if ($datapart[0]['user'] == $_SESSION['name']) {
                    $teste = TRUE;
                }
            }
            if (array_key_exists('ID', $_SESSION)) {
                if (!empty($doc)) {
                    if (($doc[0]['publico'] == 1) || ($doc[0]['publico'] == 0 && $sessionid == (int) $doc[0]['uploader_id']) || $teste == TRUE) {//precisas de fazer foreach
                        ?><article>
                            <p>Titulo: <b><?= $doc[0]['Titulo'] ?></b></p>
                            <p>Autor: <b><?= $doc[0]['Autor'] ?></b></p>
                            <p>Resumo: <b><?= $doc[0]['Resumo'] ?></b></p>
                            <p>Conteúdo: <b><?= $doc[0]['Conteudo'] ?></b></p>
                            <p>Categoria: <b><?= $doc[0]['Categoria_categoria'] ?></b></p>
                            <p>Data criacao: <b><?= $doc[0]['DataCriacao'] ?></b></p>
                            <p>Filesize: <b><?= $doc[0]['filesize'] ?> kb</b></p>
                        </article>
                        <?php for ($i = 0; $i < count($dataedits); $i++) { ?>
                            <article class="edits">
                                <p>Data edição: <b><?= $dataedits[$i]['dataedit'] ?></b></p>
                                <p>Razão: <b><?= $dataedits[$i]['razao'] ?></b></p>
                            </article>
                        <?php }
                        ?>
                    <?php } else { ?>
                        <span class="alert">Não tem permissões para visualizar o documento</span>
                        <?php
                    }
                } else {
                    ?>
                    <span class="alert">Documento não encontrado!</span>
                    <?php
                }
            } else {
                if (!empty($doc)) {
                    if ($doc[0]['publico'] == 1) {
                        ?>
                        <article>
                            <p>Titulo: <b><?= $doc[0]['Titulo'] ?></b></p>
                            <p>Autor: <b><?= $doc[0]['Autor'] ?></b></p>
                            <p>Resumo: <b><?= $doc[0]['Resumo'] ?></b></p>
                            <p>Conteúdo: <b><?= $doc[0]['Conteudo'] ?></b></p>
                            <p>Categoria: <b><?= $doc[0]['Categoria_categoria'] ?></b></p>
                            <p>Data criacao: <b><?= $doc[0]['DataCriacao'] ?></b></p>
                            <p>Filesize: <b><?= $doc[0]['filesize'] ?> kb</b></p>
                        </article>
                    <?php } else { ?>
                        <span class="alert">Não tem permissões para visualizar o documento</span>
                        <?php
                    }
                } else {
                    ?>
                    <span class="alert">Documento não encontrado!</span>
                    <?php
                }
            }
            ?>
            <div id="observacoes">
            </div>
        </div>
    </body>
</html>
