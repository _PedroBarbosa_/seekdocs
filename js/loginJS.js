/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function checkUser() {
    var userinputValue = document.getElementById('UserLogin').value;
    if (userinputValue.length < 3 || userinputValue.length > 20) {
        document.getElementById('usererror').innerHTML = "Username deverá ter entre 3 e 20 caracteres";
    } else {
        document.getElementById('usererror').innerHTML = "";
    }
}

function checkpass() {
    var passinputValue = document.getElementById('PassLogin').value;
    if (passinputValue.length < 3 || passinputValue.length > 20) {
        document.getElementById('passerror').innerHTML = "Passord deverá ter entre 3 a 20 caracteres.";
    } else {
        document.getElementById('passerror').innerHTML = "";
    }
}

function init() {
    document.getElementById('UserLogin').addEventListener('focusout', checkUser);
    document.getElementById('PassLogin').addEventListener('focusout', checkpass);
}

document.addEventListener('DOMContentLoaded', init);
