/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getUserDocs(data) {
    var arrayDocs = JSON.parse(data);
    var i = 0;

    var main = document.getElementById('main');

    for (i = 0; i < arrayDocs.length; i++) {
        var art = document.createElement('article');

        var b1 = document.createElement('b');
        var b2 = document.createElement('b');
        var b3 = document.createElement('b');
        var b4 = document.createElement('b');
        var b5 = document.createElement('b');
        var b6 = document.createElement('b');
        var b7 = document.createElement('b');

        var titulo = document.createTextNode('Titulo: ');
        var autor = document.createTextNode('Autor: ');
        var resumo = document.createTextNode('Resumo: ');
        var categoria = document.createTextNode('Categoria: ');
        var data = document.createTextNode('Data: ');
        var tags = document.createTextNode('Tags: ');
        var filesize = document.createTextNode('Filesize: ');

        b1.appendChild(titulo);
        b2.appendChild(autor);
        b3.appendChild(resumo);
        b4.appendChild(categoria);
        b5.appendChild(data);
        b6.appendChild(tags);
        b7.appendChild(filesize);

        var p1 = document.createElement('p');
        var p2 = document.createElement('p');
        var p3 = document.createElement('p');
        var p4 = document.createElement('p');
        var p5 = document.createElement('p');
        var p6 = document.createElement('p');
        var p7 = document.createElement('p');

        var tituloarray = document.createTextNode(arrayDocs[i].Titulo);
        var autorarray = document.createTextNode(arrayDocs[i].Autor);
        var resumoarray = document.createTextNode(arrayDocs[i].Resumo);
        var categoriaarray = document.createTextNode(arrayDocs[i].Categoria_categoria);
        var dataarray = document.createTextNode(arrayDocs[i].DataCriacao);
        var tagsarray = document.createTextNode(arrayDocs[i].Tags);
        var filesizearray = document.createTextNode(arrayDocs[i].filesize);

        p1.appendChild(b1);
        p1.appendChild(tituloarray);
        p2.appendChild(b2);
        p2.appendChild(autorarray);
        p3.appendChild(b3);
        p3.appendChild(resumoarray);
        p4.appendChild(b4);
        p4.appendChild(categoriaarray);
        p5.appendChild(b5);
        p5.appendChild(dataarray);
        p6.appendChild(b6);
        p6.appendChild(tagsarray);
        p7.appendChild(b7);
        p7.appendChild(filesizearray);

        art.appendChild(p1);
        art.appendChild(p2);
        art.appendChild(p3);
        art.appendChild(p4);
        art.appendChild(p5);
        art.appendChild(p6);
        art.appendChild(p7);

        var rem = document.createElement('a');
        rem.innerHTML = "Remover";
        rem.setAttribute('href', 'remdoc.php?id= ' + arrayDocs[i].id);

        var ver = document.createElement('a');
        ver.innerHTML = "Ver detalhes";
        ver.setAttribute('href', '../Ver.php?docid=' + arrayDocs[i].id);

        var edit = document.createElement('a');
        edit.innerHTML = "Editar";
        edit.setAttribute('href', 'EditDoc.php?docid=' + arrayDocs[i].id);


        art.appendChild(rem);
        art.appendChild(ver);
        art.appendChild(edit);

        main.appendChild(art);
    }
}

function init() {
    $.get('../Users/getUserDocs.php', function (data) {
        getUserDocs(data);
    }).fail(function () {
        document.getElementById('main').innerHTML = "Não foi possivel aceder aos dados";
    }
    );
}

document.addEventListener('DOMContentLoaded', init);
