/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function checkTel() {
    var telValue = document.getElementById('EditTel').value;
    if (telValue.length !== 9 || isNaN(telValue)) {
        document.getElementById('telerror').innerHTML = "Telefone inválido";
    } else {
        document.getElementById('telerror').innerHTML = "";
    }
}

function checkAddress() {
    var addressValue = document.getElementById('EditAddress').value;
    if (addressValue === "") {
        document.getElementById('addresserror').innerHTML = "Morada inválida";
    } else {
        document.getElementById('addresserror').innerHTML = "";
    }
}

function checkEmail() {
    var emailValue = document.getElementById('EditEmail').value;
    var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!pattern.test(emailValue)) {
        document.getElementById('emailerror').innerHTML = "Email com formato inválido.";
    } else {
        document.getElementById('emailerror').innerHTML = "";
    }
}

function checkPass2() {
    var PasscheckValue = document.getElementById('checkPass').value;
    var PassValue = document.getElementById('EditPass').value;
    if (!(PasscheckValue === PassValue)) {
        document.getElementById('checkpasserror').innerHTML = "As passwords não coincidem.";
    } else {
        document.getElementById('checkpasserror').innerHTML = "";
    }
}

function checkName() {
    var Namevalue = document.getElementById('EditName').value;
    if (Namevalue.length < 3 || Namevalue.length > 20) {
        document.getElementById('nameerror').innerHTML = "O nome deverá ter 3 a 20 caracteres.";
    } else {
        document.getElementById('nameerror').innerHTML = "";
    }
}

function PassValid(){
    var PassValue = document.getElementById('EditPass').value;
    if (PassValue.length < 3 || PassValue.length > 20) {
        document.getElementById('passerror').innerHTML = "A password deverá ter entre 3 e 20 caracteres.";
    }else {
        document.getElementById('passerror').innerHTML = "";
    }
}


function init() {

    var EditName = document.getElementById('EditName');
    EditName.addEventListener('blur', checkName);

    var EditPass = document.getElementById('EditPass');
    EditPass.addEventListener('blur', PassValid);

    var checkPass = document.getElementById('checkPass');
    checkPass.addEventListener('blur', checkPass2);

    var EditEmail = document.getElementById('EditEmail');
    EditEmail.addEventListener('blur', checkEmail);

    var EditAddress = document.getElementById('EditAddress');
    EditAddress.addEventListener('blur', checkAddress);

    var EditTel = document.getElementById('EditTel');
    EditTel.addEventListener('blur', checkTel);
}

document.addEventListener('DOMContentLoaded', init);

