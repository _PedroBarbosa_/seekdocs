/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function checkautor() {

    var autor = document.getElementById('Autorinput').value;
    if (autor.length < 3 || autor.length > 20) {
        document.getElementById('autorerror').innerHTML = "Autor inválido!";
    } else {
        document.getElementById('autorerror').innerHTML = "";
    }
}

function checktags() {
    var tags = document.getElementById('tagsinput').value;
    if (tags.length <= 0) {
        document.getElementById('tagserror').innerHTML = "Introduza tags";
    } else {
        document.getElementById('tagserror').innerHTML = "";
    }
}

function validate_fileupload(fileName)
{
    var allowed_extensions = new Array("doc", "docx");
    var file_extension = fileName.split('.').pop(); // split function will split the filename by dot(.), and pop function will pop the last element from the array which will give you the extension as well. If there will be no extension then it will return the filename.

    for (var i = 0; i <= allowed_extensions.length; i++)
    {
        if (allowed_extensions[i] == file_extension)
        {
            return true; // valid file extension
        }
    }

    return false;
}

function checkfile() {
    var file = document.getElementById('fileinput').value;
    if (!validate_fileupload(file)) {
        document.getElementById('fileerror').innerHTML = "ficheiro inválido!";
    } else {
        document.getElementById('fileerror').innerHTML = "";
    }

}

function disablepart() {
    var partilha = document.getElementById('partilha');
    var box = document.getElementById('publico');

    if (box.checked) {
        partilha.disabled = true;
    } else {
        partilha.disabled = false;
    }
}

function init() {

    document.getElementById('Autorinput').addEventListener('blur', checkautor);
    document.getElementById('tagsinput').addEventListener('blur', checktags);
    document.getElementById('fileinput').addEventListener('upload', checkfile);
    document.getElementById('publico').addEventListener('click', disablepart);
}

document.addEventListener('DOMContentLoaded', init);
