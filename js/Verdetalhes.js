/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var comentarios = [];

function getParameterByName(name, url) {
    if (!url)
        url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function carregarComentariosStorage() {
    if (typeof (localStorage) !== "undefined") {
        try {
            var commentsTemp = localStorage.getItem('comentarios');
            if (commentsTemp !== null) {
                comentarios = JSON.parse(commentsTemp);
                popularComentariosDOM(comentarios);
            }
        } catch (err) {
        }
    } else {
        console.error('Sorry! No Web Storage support..');
    }
}

function popularComentariosDOM(data) {
    var iddoc = getParameterByName('docid');

    var i = 0;
    for (i = 0; i < data.length; ++i) {
        if (iddoc === data[i].id) {
            loadComment(data[i].comment);
        }
    }
}

function loadComment(coment) {
    var obsart = document.createElement('article');
    obsart.setAttribute('class', 'comment');

    var inputtext = document.createTextNode(coment);
    obsart.appendChild(inputtext);
    document.getElementById('observacoes').appendChild(obsart);
}

function guardarComentariosStorage() {
    if (typeof (localStorage) !== "undefined") {
        try {
            localStorage.setItem('comentarios', JSON.stringify(comentarios));
        } catch (err) {
            if (err == QUOTA_EXCEEDED_ERR) {
                console.error('Limite da localStorage excedido!!!');
            } else {
                console.error('Erro ao guardar para o localStorage');
            }
        }
    } else {
        console.error('Sorry! No Web Storage support..');
    }
}

function addcomment() {
    var obsart = document.createElement('article');
    obsart.setAttribute('class', 'comment');
    var input = document.getElementById('inputobs').value;
    var inputtext = document.createTextNode(input);
    var iddoc = getParameterByName('docid');
    var obj = {id: iddoc, comment: input};
    obsart.appendChild(inputtext);
    document.getElementById('observacoes').appendChild(obsart);

    comentarios.push(obj);

    guardarComentariosStorage();
}

function init() {
    carregarComentariosStorage();
    var obsdiv = document.getElementById('observacoes');
    var obsinptut = document.createElement('input');
    obsinptut.setAttribute('placeholder', 'Observação..');
    obsinptut.setAttribute('id', 'inputobs');
    obsdiv.appendChild(obsinptut);
    var obsbutton = document.createElement('button');
    obsbutton.innerHTML = "Adicionar observação";
    obsdiv.appendChild(obsbutton);
    obsbutton.addEventListener('click', addcomment);
}

document.addEventListener('DOMContentLoaded', init);