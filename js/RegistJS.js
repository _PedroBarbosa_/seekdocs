/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function checkTel() {
    var telValue = document.getElementById('RegistTel').value;
    if (telValue.length !== 9 || isNaN(telValue)) {
        document.getElementById('telerror').innerHTML = "Telefone inválido";
    } else {
        document.getElementById('telerror').innerHTML = "";
    }
}

function checkAddress() {
    var addressValue = document.getElementById('RegistAddress').value;
    if (addressValue === "") {
        document.getElementById('addresserror').innerHTML = "Morada inválida";
    } else {
        document.getElementById('addresserror').innerHTML = "";
    }
}

function checkEmail() {
    var emailValue = document.getElementById('RegistEmail').value;
    var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!pattern.test(emailValue)) {
        document.getElementById('emailerror').innerHTML = "Email com formato inválido.";
    } else {
        document.getElementById('emailerror').innerHTML = "";
    }
}

function checkPass2() {
    var PasscheckValue = document.getElementById('RegistcheckPass').value;
    var PassValue = document.getElementById('RegistPass').value;
    if (!(PasscheckValue === PassValue)) {
        document.getElementById('checkpasserror').innerHTML = "As passwords não coincidem.";
    } else {
        document.getElementById('checkpasserror').innerHTML = "";
    }
}

function checkPass() {
    var PassValue = document.getElementById('RegistPass').value;
    if (PassValue.length < 3 || PassValue > 20) {
        document.getElementById('passerror').innerHTML = "A password deverá ter entre 3 e 20 caracteres.";
    } else {
        document.getElementById('passerror').innerHTML = "";
    }
}

function checkName() {
    var Namevalue = document.getElementById('RegistName').value;
    if (Namevalue.length < 3 || Namevalue.length > 20) {
        document.getElementById('nameerror').innerHTML = "O nome deverá ter 3 a 20 caracteres.";
    } else {
        document.getElementById('nameerror').innerHTML = "";
    }
}

function init() {

    var RegistName = document.getElementById('RegistName');
    RegistName.addEventListener('blur', checkName);

    var RegistPass = document.getElementById('RegistPass');
    RegistPass.addEventListener('focusout', checkPass);

    var RegistPass2 = document.getElementById('RegistcheckPass');
    RegistPass2.addEventListener('focusout', checkPass2);

    var RegistEmail = document.getElementById('RegistEmail');
    RegistEmail.addEventListener('focusout', checkEmail);

    var RegistAddress = document.getElementById('RegistAddress');
    RegistAddress.addEventListener('focusout', checkAddress);

    var RegistTel = document.getElementById('RegistTel');
    RegistTel.addEventListener('focusout', checkTel);
}

document.addEventListener('DOMContentLoaded', init);

