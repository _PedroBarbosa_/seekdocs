<?php

require_once '../Application/Manager/RegisteredUserManager.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

$man = new RegisteredUserManager();

try {
    $man->DeleteUserByID($id);
} catch (Exception $ex) {
    header('location:GerirUtilizadores.php?failRemove=TRUE');
}

header('location:GerirUtilizadores.php?successRemove=TRUE');




