<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="../styles/styleHome.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <header>
            <h1>Administration</h1>
        </header>
        <div id="main">
            <img src="../Images/icon.png" alt="Logo icon" class="imgHome">
            <form method="POST" action="checkAdminLogin.php">
                <?php if (filter_has_var(INPUT_GET, 'errorAuth') == TRUE) { ?>
                    <span class="error">Login incorreto</span><?php
                }
                ?>
                <label><input type="text" name="AdminName" placeholder="Username.." maxlength="15" autofocus required class="inputtype" id="AdminName"></label><span class="error" id="usererror"></span>
                <label><input type="password" name="AdminPass" placeholder="Password.." maxlength="20" required class="inputtype" id="AdminPass"></label><span class="error" id="passerror"></span>
                <input type="submit" value="Entrar" class="inputsubmit">
            </form>
            <a href="../index.php" id="voltar">Voltar</a>
        </div>
    </body>
</html>
