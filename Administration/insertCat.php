<?php

require_once '../Application/Manager/CatManager.php';

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (filter_has_var(INPUT_GET,'addCat')) {
    $cat = filter_input(INPUT_GET, 'addCat', FILTER_SANITIZE_STRING, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    if(strlen($cat) < 3 || $cat === "" || empty($cat)){
        $errors = "categoria inválida";
    }
}
//
//var_dump($cat);
//var_dump($errors);
//
if(!isset($errors)){
    $catman = new CatManager();
    $catman->insertCat($cat);
    header('location:gerirCat?insertsuccess=TRUE');
}else{
    header('location:gerirCat?insertfailed=TRUE');
}