<?php
session_start();

if (isset($_SESSION['AdminName']) && $_SESSION['AdminName'] != '') {
    if (array_key_exists('AdminName', $_SESSION)) {
        
    }
} else {
    header('location:../index.php?gtfo=yes');
}
require_once '../Application/Model/RegisteredUser.php';
require_once '../Application/Manager/RegisteredUserManager.php';


$man = new RegisteredUserManager();
$data = $man->getAllUsers();
?>

<!DOCTYPE html>

<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Gerir utilizadores</title>
        <link href="../styles/Mainstyle.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <header>
            <a href="../Logout.php" >Logout</a>
        </header>
        <nav id="menu">
            <a href="Administration.php">Aceitar utilizadores</a>
            <a href="GerirUtilizadores.php">Gerir utilizadores</a>
            <a href="gerirCat.php">Gerir categorias</a>
        </nav>
        <div id="main">
            <?php if (filter_has_var(INPUT_GET, 'successRemove') == TRUE) { ?>
                <span class="alert">Removido com sucesso!</span>
                <?php
            }
            if (filter_has_var(INPUT_GET, 'failRemove') == TRUE) {
                ?>
                <span class="alert">Não foi possivel remover</span>
                <?php
            }
            if (filter_has_var(INPUT_GET, 'deactivateSuccess') == TRUE) {
                ?>
                <span class="alert">Utilizador desativado com sucesso</span>
            <?php }if (filter_has_var(INPUT_GET, 'deactivateFail') == TRUE) {
                ?>
                <span class="alert">Utilizador não desativado</span>
                <?php
            }
            if (filter_has_var(INPUT_GET, 'activateSuccess') == TRUE) {
                ?>
                <span class="alert">Utilizador ativado com sucesso!</span>
            <?php }if (filter_has_var(INPUT_GET, 'activateFail') == TRUE) {
                ?>
                <span class="alert">Utilizador não ativado</span>
                <?php
            }
            if (filter_has_var(INPUT_GET, 'errorid') == TRUE) {
                ?>
                <span class="alert">Utilizador não existe</span>
                <?php
            }
            if (empty($data)) {
                ?>
                <span class="alert">não exite utilizadores registados na BD</span>
            <?php }
            ?>
            <?php for ($i = 0; $i < count($data); $i++) { ?>
                <article>
                    <p>ID: <b><?= $data[$i]['ID'] ?></b></p>
                    <p>Utilizador: <b><?= $data[$i]['User'] ?></b></p>
                    <p>País: <b><?= $data[$i]['Pais'] ?></b></p>
                    <p>Morada: <b><?= $data[$i]['address'] ?></b></p>
                    <p>Telefone: <b><?= $data[$i]['tel'] ?></b></p>
                    <p>Data registo: <b><?= $data[$i]['Registdate'] ?></b></p>

                    <div class="controls">
                        <?php if ($data[$i]['ativo'] == 1) { ?>
                            <a href="DesativarUser.php?id=<?= $data[$i]['ID'] ?>">Desativar</a>
                        <?php } else { ?>
                            <a href="AtivarUser.php?id=<?= $data[$i]['ID'] ?>">Ativar</a>
                        <?php }
                        ?>
                        <a href="BanUser.php?id=<?= $data[$i]['ID'] ?>">Ban</a>
                    </div>
                </article>
            <?php } ?>
        </div>
    </body>
</html>
