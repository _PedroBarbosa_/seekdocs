<?php

require_once '../Application/Manager/UnregisteredUserManager.php';
require_once '../Application/Model/UnregisteredUser.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$ID = filter_input(INPUT_GET, 'id');

$manUn = new UnregisteredUserManager();

$temp = $manUn->DeleteUserById($ID);

if (empty($temp)) {
    header('location:Administration.php?errorid=TRUE');
} else {
    header('location:Administration.php?sucessRemove=TRUE');
}