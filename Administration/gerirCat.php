<?php
session_start();

if (isset($_SESSION['AdminName']) && $_SESSION['AdminName'] != '') {
    if (array_key_exists('AdminName', $_SESSION)) {
        
    }
} else {
    header('location:../index.php?gtfo=yes');
}
require_once '../Application/Manager/CatManager.php';


$man = new CatManager();
$data = $man->getAllCat();
?>

<!DOCTYPE html>

<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Administration</title>
        <link href="../styles/Mainstyle.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <header>
            <a href="../Logout.php" >Logout</a>
        </header>
        <nav id="menu">
            <a href="Administration.php">Aceitar utilizadores</a>
            <a href="GerirUtilizadores.php">Gerir utilizadores</a>
            <a href="gerirCat.php">Gerir categorias</a>
        </nav>
        <div id="main">
            <?php if (filter_has_var(INPUT_GET, 'insertsuccess') === TRUE) { ?>
                <span class="alert">inserido com sucesso!</span>
                <?php
            }
            if (filter_has_var(INPUT_GET, 'insertfailed') === TRUE) {
                ?>
                <span class="alert">Insira uma categoria válida!</span>
                <?php
            }
            if (filter_input(INPUT_GET, 'remove') == "success") {
                ?>
                <span class="alert">removido com sucesso!</span>
                <?php
            }
            if (empty($data)) {
                ?>
                <span class="alert">Não existe categorias</span>
            <?php }
            ?>
            <?php for ($i = 0; $i < count($data); $i++) { ?>
                <article>
                    <p>Categoria: <b><?= $data[$i]['categoria'] ?></b></p>
                    <div class="controls">
                        <a href="removeCat.php?cat=<?= $data[$i]['categoria'] ?>">Remover</a>
                    </div>
                </article>
            <?php } ?>
            <form method="GET" action="insertCat.php">
                <input type="text" name="addCat">
                <input type="submit" value="Adicionar Categoria">
            </form>
        </div>
    </body>
</html>