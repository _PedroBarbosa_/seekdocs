<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once '../Config.php';
require_once '../Application/Manager/AdminManager.php';

$man = new AdminManager();
$test = $man->CheckAuth(filter_input(INPUT_POST,'AdminName'),filter_input(INPUT_POST,'AdminPass'));

if ($test == true){
    session_start();
    $_SESSION['AdminName'] = filter_input(INPUT_POST,'AdminName');
    header('location:Administration.php');
} else {
    header("location:Admin.php?errorAuth=TRUE");
}