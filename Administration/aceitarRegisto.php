<?php

require_once '../Application/Manager/RegisteredUserManager.php';
require_once '../Application/Model/RegisteredUser.php';

require_once '../Application/Manager/UnregisteredUserManager.php';
require_once '../Application/Model/UnregisteredUser.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$id = filter_input(INPUT_GET, 'id',FILTER_SANITIZE_NUMBER_INT);

$manUn = new UnregisteredUserManager();
$manRe = new RegisteredUserManager();

$regUser = new RegisteredUser();
$copy = $manUn->getUserById($id);

if(empty($copy)){
    header('location:Administration.php?errorid=TRUE');
}

$fvf = $regUser->convertArrayToObject($copy[0]);

$manRe->insertUser($fvf);

$temp = $manUn->DeleteUserById($id);

header('location:Administration.php?successAccept=TRUE');