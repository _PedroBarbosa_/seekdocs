<?php
session_start();

if (isset($_SESSION['AdminName']) && $_SESSION['AdminName'] != '') {
    if (array_key_exists('AdminName', $_SESSION)) {
        
    }
} else {
    header('location:../index.php?gtfo=yes');
}
require_once '../Application/Model/UnregisteredUser.php';
require_once '../Application/Manager/UnregisteredUserManager.php';


$man = new UnregisteredUserManager();
$data = $man->getAlluser();
?>

<!DOCTYPE html>

<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Administration</title>
        <link href="../styles/Mainstyle.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <header>
            <a href="../Logout.php" >Logout</a>
        </header>
        <nav id="menu">
            <a href="Administration.php">Aceitar utilizadores</a>
            <a href="GerirUtilizadores.php">Gerir utilizadores</a>
            <a href="gerirCat.php">Gerir categorias</a>
        </nav>
        <div id="main">
            <?php if (filter_has_var(INPUT_GET, 'sucessRemove') == TRUE) { ?>
                <span class="alert">Removido com sucesso!</span>
                <?php
            }
            if (filter_has_var(INPUT_GET, 'successAccept') == TRUE) {
                ?>
                <span class="alert">Aceite com sucesso!</span>
                <?php
            }
            if(filter_has_var(INPUT_GET,'errorid') == TRUE){?>
                <span class="alert">Utilizador não existe</span>
            <?php }
            if (empty($data)) {
                ?>
                <span class="alert">não existe utilizadores para aceitar</span>
            <?php }
            ?>
            <?php for ($i = 0; $i < count($data); $i++) { ?>
                <article>
                    <p>ID: <b><?= $data[$i]['ID'] ?></b></p>
                    <p>Utilizador: <b><?= $data[$i]['User'] ?></b></p>
                    <p>País: <b><?= $data[$i]['Pais'] ?></b></p>
                    <p>Morada: <b><?= $data[$i]['address'] ?></b></p>
                    <p>Telefone: <b><?= $data[$i]['tel'] ?></b></p>
                    <p>Data registo: <b><?= $data[$i]['Registdate'] ?></b></p>

                    <div class="controls">
                        <a href="aceitarRegisto.php?id=<?= $data[$i]['ID'] ?>">Aceitar</a>
                        <a href="removeUnregistedUser.php?id=<?= $data[$i]['ID'] ?>">Rejeitar</a>
                    </div>
                </article>
            <?php } ?>
        </div>
    </body>
</html>
