<?php

require_once '../Application/Manager/RegisteredUserManager.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

$man = new RegisteredUserManager();
$RegUser = new RegisteredUser();
$data = $man->getUserById($id);

if (empty($data)) {
    header('location:GerirUtilizadores.php?errorid=TRUE');
}

$user = $RegUser->convertArrayToObject($data[0]);
$user->setAtivo(1);

try {
    $man->EditUserById($user);
} catch (Exception $ex) {
    header('location:GerirUtilizadores.php?activateFail=TRUE');
    throw $ex;
}

header('location:GerirUtilizadores.php?activateSuccess=TRUE');
